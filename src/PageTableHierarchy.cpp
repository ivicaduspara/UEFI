#include "../include/utility/PageTableHierarchy.h"

PageTableHierarchy::PageTableHierarchy(EFI_SYSTEM_TABLE *SystemTable) {
  /*1.) Allocate 4 pages. These are used for:
   * + PML4 table
   * + PML3 table
   * + PML2 table
   * + PML1 table
   * */
   EFI_STATUS status;
   //As no specific address is required, EFI_ALLOCATE_TYPE::AllocateMaxAddress will be used for allocation.
   UINT64 PML4 = kernel_max_alloc_address;
   UINT64 PML3 = kernel_max_alloc_address;
   UINT64 PML2 = kernel_max_alloc_address;
   UINT64 PML1 = kernel_max_alloc_address;

   status = SystemTable->BootServices->AllocatePages(EFI_ALLOCATE_TYPE::AllocateMaxAddress,
                                                     EFI_MEMORY_TYPE::EfiLoaderData,
                                                     1ULL,
                                                     &PML4);
   if (status != EFI_SUCCESS) {
     print_error(SystemTable->ConOut,
                 CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                 L"Failed to allocate a page for PML4 table.\r\nEFI error: "),
                 status);
     return;
   } else {
     SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Allocated PML4 @: "));
     print_unsigned_number(SystemTable->ConOut, PML4);
     SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" | "));
     print_unsigned_hex_number(SystemTable->ConOut, PML4);
     SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
   }

   status = SystemTable->BootServices->AllocatePages(EFI_ALLOCATE_TYPE::AllocateMaxAddress,
                                                     EFI_MEMORY_TYPE::EfiLoaderData,
                                                     1ULL,
                                                     &PML3);
   if (status != EFI_SUCCESS) {
     print_error(SystemTable->ConOut,
                 CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                 L"Failed to allocate a page for PML3 table.\r\nEFI error: "),
                 status);
     return;
   }

   status = SystemTable->BootServices->AllocatePages(EFI_ALLOCATE_TYPE::AllocateMaxAddress,
                                                     EFI_MEMORY_TYPE::EfiLoaderData,
                                                     1ULL,
                                                     &PML2);
   if (status != EFI_SUCCESS) {
     print_error(SystemTable->ConOut,
                 CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                 L"Failed to allocate a page for PML2 table.\r\nEFI error: "),
                 status);
     return;
   }

   status = SystemTable->BootServices->AllocatePages(EFI_ALLOCATE_TYPE::AllocateMaxAddress,
                                                     EFI_MEMORY_TYPE::EfiLoaderData,
                                                     1ULL,
                                                     &PML1);
   if (status != EFI_SUCCESS) {
     print_error(SystemTable->ConOut,
                 CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                 L"Failed to allocate a page for PML1 table.\r\nEFI error: "),
                 status);
     return;
   }

   //2.) Zero out every allocated page. I don't know if it is necessary, but to be safe...
   auto* PML4_address_as_ptr = reinterpret_cast<UINT64*>(PML4);
   auto* PML3_address_as_ptr = reinterpret_cast<UINT64*>(PML3);
   auto* PML2_address_as_ptr = reinterpret_cast<UINT64*>(PML2);
   auto* PML1_address_as_ptr = reinterpret_cast<UINT64*>(PML1);

   for(UINTN i = 0; i < 512; i++) {
     PML4_address_as_ptr[i] = 0ull;
     PML3_address_as_ptr[i] = 0ull;
     PML2_address_as_ptr[i] = 0ull;
     PML1_address_as_ptr[i] = 0ull;
   }

   /* 3.) Hook pages together. PML4[511]->PML3[1]->PML2[0]->PML1
     * Mark pages as present, writeable & global.
     * Global because TLB will be invalidated after UEFI ends.
     * */
   PML4_address_as_ptr[511] = (PML3 & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;
   PML3_address_as_ptr[1] = (PML2 & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;
   PML2_address_as_ptr[0] = (PML1 & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;

   /*4.) Because of trampoline & higher half kernel, some identity mapping must be preserved.
    * Allocate an PML3 table which will represent lower 4 GB. Hook it to PML[0]
    * Use HUGE page to avoid extra PML2 & PML1 tables
    * */
   PML3 = kernel_max_alloc_address;
   status = SystemTable->BootServices->AllocatePages(EFI_ALLOCATE_TYPE::AllocateMaxAddress,
                                                     EFI_MEMORY_TYPE::EfiLoaderData,
                                                     1,
                                                     &PML3);
   ZeroOut(reinterpret_cast<UINT8*>(PML3), 4096);
   if (status != EFI_SUCCESS) {
     print_error(SystemTable->ConOut,
                 CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                     L"Failed to allocate a page for PML3 table used in identity mapping.\r\nEFI error: "),
                 status);
     return;
   }

   PML3_address_as_ptr = reinterpret_cast<UINT64*>(PML3);
   PML4_address_as_ptr[0] = (PML3 & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;
   for(UINTN i = 0; i < 4; i++) {
     PML3_address_as_ptr[i] = ( i * (1 << 30) ) | PageFlags::Global | PageFlags::Size | PageFlags::Write | PageFlags::Present;
   }

   PML4_pointer = PML4_address_as_ptr;
}

EFI_STATUS PageTableHierarchy::MapSinglePage(EFI_SYSTEM_TABLE* SystemTable,
                                             UINT64 PhysicalAddress, UINT64 VirtualAddress, UINT64 flags,
                                             EFI_ALLOCATE_TYPE allocation_type) {
  EFI_STATUS status;
/*
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\tPA: "));
  print_unsigned_hex_number(SystemTable->ConOut, PhysicalAddress);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" | VA: "));
  print_unsigned_hex_number(SystemTable->ConOut, VirtualAddress);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
*/

  //1.) Obtain appropriate indexes for entries.
  UINT64 PML4_index = (VirtualAddress >> PML4_INDEX_SHIFT) & PML_INDEX_MASK;
  UINT64 PML3_index = (VirtualAddress >> PML3_INDEX_SHIFT) & PML_INDEX_MASK;
  UINT64 PML2_index = (VirtualAddress >> PML2_INDEX_SHIFT) & PML_INDEX_MASK;
  UINT64 PML1_index = (VirtualAddress >> PML1_INDEX_SHIFT) & PML_INDEX_MASK;

/*  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Indexes: "));
  print_unsigned_number(SystemTable->ConOut, PML4_index);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L","));
  print_unsigned_number(SystemTable->ConOut, PML3_index);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L","));
  print_unsigned_number(SystemTable->ConOut, PML2_index);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L","));
  print_unsigned_number(SystemTable->ConOut, PML1_index);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));*/

  UINT64 PML3_address = kernel_max_alloc_address;
  UINT64 PML2_address = kernel_max_alloc_address;
  UINT64 PML1_address = kernel_max_alloc_address;

  if (!(PML4_pointer[PML4_index] & PageFlags::Present)) {

    status = SystemTable->BootServices->AllocatePages(allocation_type,
                                                      EFI_MEMORY_TYPE::EfiLoaderData,
                                                      1ULL,
                                                      &PML3_address);
    ZeroOut(reinterpret_cast<UINT8*>(PML3_address), X86_64_PAGE_SIZE);
    if (status != EFI_SUCCESS) {
      print_error(SystemTable->ConOut,
                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                   L"Failed to allocate a page for a PML3 table when mapping a single page.\r\nEFI error: "),
                   status);
      return status;
    }
    PML4_pointer[PML4_index] = (PML3_address & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;
  }

  auto* PML3_pointer = reinterpret_cast<UINT64*>(PML4_pointer[PML4_index] & PML_ENTRY_MASK);

  if (!(PML3_pointer[PML3_index] & PageFlags::Present)) {
    status = SystemTable->BootServices->AllocatePages(allocation_type,
                                                      EFI_MEMORY_TYPE::EfiLoaderData,
                                                      1ULL,
                                                      &PML2_address);
    ZeroOut(reinterpret_cast<UINT8*>(PML2_address), X86_64_PAGE_SIZE);

    if (status != EFI_SUCCESS) {
      print_error(SystemTable->ConOut,
                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                   L"Failed to allocate a page for a PML2 table when mapping a single page.\r\nEFI error: "),
                  status);
      return status;
    }
    PML3_pointer[PML3_index] = (PML2_address & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;
  }

  auto* PML2_pointer = reinterpret_cast<UINT64*>(PML3_pointer[PML3_index] & PML_ENTRY_MASK);


  if (!(PML2_pointer[PML2_index] & PageFlags::Present)) {
    status = SystemTable->BootServices->AllocatePages(allocation_type,
                                                      EFI_MEMORY_TYPE::EfiLoaderData,
                                                      1ULL,
                                                      &PML1_address);
    ZeroOut(reinterpret_cast<UINT8*>(PML1_address), X86_64_PAGE_SIZE);

    if (status != EFI_SUCCESS) {
      print_error(SystemTable->ConOut,
                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                      L"Failed to allocate a page for a PML1 table when mapping a single page.\r\nEFI error: "),
                  status);
      return status;
    }
    PML2_pointer[PML2_index] = (PML1_address & PML_ENTRY_MASK) | PageFlags::Global | PageFlags::Write | PageFlags::Present;
  }

  auto* PML1_pointer = reinterpret_cast<UINT64*>(PML2_pointer[PML2_index] & PML_ENTRY_MASK);
  if (PML1_pointer[PML1_index] & PageFlags::Present) {
    status = EFI_ABORTED;
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"When mapping a virtual address, already existing mapping "
                                                        "was discovered! Aborting.\r\nEFI error: "),
                status);
  }
  //There is no need to allocate a page for PhysicalAddress. It is assumed that it is already allocated.
  PML1_pointer[PML1_index] = (PhysicalAddress & PML_ENTRY_MASK) | PageFlags::Global | flags | PageFlags::Present;
  return EFI_SUCCESS;
}

EFI_STATUS PageTableHierarchy::MapPages(EFI_SYSTEM_TABLE *SystemTable, UINT64 count,
                                        UINT64 physical_address, UINT64 virtual_address, UINT64 flags,
                                        EFI_ALLOCATE_TYPE allocation_type) {
  EFI_STATUS status = EFI_SUCCESS;
  while (count > 0) {
    status = MapSinglePage(SystemTable, physical_address, virtual_address, flags, allocation_type);
    if (status != EFI_SUCCESS) {
      print_error(SystemTable->ConOut,
                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Mapping multiple pages failed.\r\nEFI error: "),
                  status);
      return status;
    }
    physical_address += X86_64_PAGE_SIZE;
    virtual_address += X86_64_PAGE_SIZE;
    count--;
  }
  return status;
}


void PageTableHierarchy::MapKernelPages(EFI_SYSTEM_TABLE *SystemTable, PT_LOAD_Descriptor *descriptors) {
  /*For every page of every PT_LOAD descriptor, an entry in appropriate page tables is required.
   * If a PT_LOAD descriptor has just one page, things are pretty straight forward.
   * Otherwise, `page_count` number of entries have to be created. The fact that these are contiguous in memory
   * can be leveraged here.
   */
  PT_LOAD_Descriptor* descriptor_iterator = descriptors;
  EFI_STATUS status = EFI_SUCCESS;
  while(descriptor_iterator != nullptr) {
    status = MapPages(SystemTable, descriptor_iterator->number_pages, descriptor_iterator->physical_address,
             descriptor_iterator->virtual_address, descriptor_iterator->flags, EFI_ALLOCATE_TYPE::AllocateAddress);
    if (status != EFI_SUCCESS) {
      print_error(SystemTable->ConOut,
                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Error mapping kernel pages\r\nEFI error: "),
                  status);
      return;
    }
    descriptor_iterator = descriptor_iterator->next_descriptor;
  }
}

void PageTableHierarchy::MapEntireRAM(EFI_SYSTEM_TABLE *SystemTable) noexcept {
  /*Let's take a look at an example. Assume that a computer has 16 GB of RAM
   * That is 2^34 B of memory.
   *
   * How many page tables (and of what kinds) are needed?
   * A page has a size of 4096B (2^12)B. The total number of pages in such computer is
   * 2^34 B / 2^12 B = 2^22
   *
   * Each PT (PML1) table can fit 512 entries or 512 pages. The total number of PTs needed is:
   * 2^22 / 2^9 = 2^13
   *
   * Each PDT (PML2) table can fit 512 entries. The total number of PDTs needed is:
   * 2^13 / 2^9 = 2^4
   *
   * Each PDPT (PML3) table can fit 512 entries. To fit 16 PDT entries, 1 PDPT table is required.
   *
   * Finally a single PML4 table is required.
   *
   * Every table is 4096B in size. The total size needed for mapping entire memory is:
   *
   * 4096B + 4096B + 16 * 4096B + 8192 * 4096B = 33 628 160 B => 32 840 KB => 32,0703125 MB
   * */

  //1.) Obtain memory map.
  EFI_STATUS status;
  UINTN                  MemoryMapSize = 0;
  EFI_MEMORY_DESCRIPTOR* MemoryMap;
  UINTN                  MapKey;
  UINTN                  DescriptorSize;
  UINT32                 DescriptorVersion;

  //The first call will fail on purpose, returns needed memory map size
  SystemTable->BootServices->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion); //NOLINT
  MemoryMapSize += 2 * DescriptorSize;
  SystemTable->BootServices->AllocatePool(EfiLoaderData, MemoryMapSize, reinterpret_cast<void**>(&MemoryMap));
  status = SystemTable->BootServices->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion);

  if(status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Failed to obtain a memory map when allocating physical page objects.\r\n"),
                status);
    //TODO Handle failure.
    return;
  }

  //2.) Calculate the total size of the available memory.
  /* There is a problem when calculating total number of bytes of RAM available.
     Essentially, there are holes in the RAM which are not returned by GetMemoryMap();
     Here is an example:
     Let D1 be descriptor number 0, or the first descriptor with the following properties:
     D1:
        PhysicalStart: 4096
        Number of pages: 159

    With some simple math, the end of the descriptor D1 is: 4096 + 159 * 4096 = 655 360
    The descriptor D2 DOESN'T necessarily start at 655 360. There could be a hole of some size, i.e.
    D2:
       PhysicalStart: 1048576
       Number of pages: 66


    To calculate total amount of memory (both holes in RAM which GetMemoryMap didn't return and available memory),
    the largest physical address must be located. This is not necessarily the last descriptor in the array.
    When the descriptor with the largest physical address is located, the following formula can be used:

    Total_RAM_in_B = LMD->PhysicalStart + 4096 * LMD->NumberOfPages;

    where LMD is the memory descriptor with the largest physical address
   * */


  UINTN number_of_entries = MemoryMapSize / DescriptorSize;
  EFI_PHYSICAL_ADDRESS largest_address = 0;
  UINTN pages_in_largest_address_desc = 0;
  for (UINTN i = 0; i < number_of_entries; i++) {
    auto* descriptor = reinterpret_cast<EFI_MEMORY_DESCRIPTOR*>(reinterpret_cast<UINT64>(MemoryMap) + (i * DescriptorSize));
    if (descriptor->PhysicalStart >= largest_address) {
      largest_address = descriptor->PhysicalStart;
      pages_in_largest_address_desc = descriptor->NumberOfPages;
    }
  }
  UINTN total_size_in_B = largest_address + X86_64_PAGE_SIZE * pages_in_largest_address_desc;


  /*3.) Map entire RAM
  NOTE: This means that the kernel's pages will be mapped twice. Once here because they are in the RAM
  once in the PML[511]-> tree. This could be troublesome. When preparing physical page struct objects,
  pages of PT_LOAD sections should be marked as taken (allocated). This is done in PreparePhysicalPageObjects function
   */
  UINTN number_of_pages = total_size_in_B / 4096;
  UINT64 flags = PageFlags::Present | PageFlags::Global;
  status = MapPages(SystemTable, number_of_pages, 0ULL, RAM_MAP_START, flags, AllocateMaxAddress);
  if (status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Failed to map entire RAM.\r\n"),
                status);
    //TODO Handle failure.
    return;
  }
}

void PageTableHierarchy::DebugPrintPageHierarchy(EFI_SYSTEM_TABLE *SystemTable) noexcept {
  for (UINTN PML4_index = 0ULL; PML4_index < 512; PML4_index++) {
    if (PML4_pointer[PML4_index] & PageFlags::Present) {
      SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                        CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Detected an entry in PML4 table at index: "));
      print_unsigned_number(SystemTable->ConOut, PML4_index);
      SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                        CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
      auto* PML3_pointer = reinterpret_cast<UINT64*>(PML4_pointer[PML4_index] & PML_ENTRY_MASK);
      for (UINTN PML3_index = 0ULL; PML3_index < 512; PML3_index++) {
        if (PML3_pointer[PML3_index] & PageFlags::Present) {
          SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                            CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\tDetected an entry in PML3 table at index: "));
          print_unsigned_number(SystemTable->ConOut, PML3_index);
          SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                            CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
          if (PML3_pointer[PML3_index] & PageFlags::Size) {
            SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                              CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"It's a huge page.\r\n"));
          } else {
            auto* PML2_pointer = reinterpret_cast<UINT64*>(PML3_pointer[PML3_index] & PML_ENTRY_MASK);
            for (UINTN PML2_index = 0ULL; PML2_index < 512; PML2_index++) {
              if (PML2_pointer[PML2_index] & PageFlags::Present) {
                SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\t\tDetected an entry in PML2 table at index: "));
                print_unsigned_number(SystemTable->ConOut, PML2_index);
                SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
                if (PML2_pointer[PML2_index] & PageFlags::Size) {
                  SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\t\tIt's a large page\r\n"));
                } else {
                  auto* PML1_pointer = reinterpret_cast<UINT64*>(PML2_pointer[PML2_index] & PML_ENTRY_MASK);
                  for (UINTN PML1_index = 0ULL; PML1_index < 512; PML1_index++) {
                    if (PML1_pointer[PML1_index] & PageFlags::Present) {
                      SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                        CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\t\t\tDetected an entry in PML1 table at index: "));
                      print_unsigned_number(SystemTable->ConOut, PML1_index);
                      SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                        CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void PageTableHierarchy::DebugPrintPML4SubtreeAtIndex(EFI_SYSTEM_TABLE *SystemTable, UINTN PML4_index) noexcept {
  if (PML4_pointer[PML4_index] & PageFlags::Present) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                      CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Detected an entry in PML4 table at index: "));
    print_unsigned_number(SystemTable->ConOut, PML4_index);
    SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                      CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
    auto* PML3_pointer = reinterpret_cast<UINT64*>(PML4_pointer[PML4_index] & PML_ENTRY_MASK);
    SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                      CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"PML3_pointer (masked) has the following value: "));
    print_unsigned_hex_number(SystemTable->ConOut, reinterpret_cast<UINT64>(PML3_pointer));
    SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                      CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
/*    for (UINTN PML3_index = 0ULL; PML3_index < 512; PML3_index++) {
      if (PML3_pointer[PML3_index] & PageFlags::Present) {
        SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                          CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\tDetected an entry in PML3 table at index: "));
        print_unsigned_number(SystemTable->ConOut, PML3_index);
        SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                          CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
        if (PML3_pointer[PML3_index] & PageFlags::Size) {
          SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                            CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"It's a huge page.\r\n"));
        } else {
          auto* PML2_pointer = reinterpret_cast<UINT64*>(PML3_pointer[PML3_index] & PML_ENTRY_MASK);
          for (UINTN PML2_index = 0ULL; PML2_index < 512; PML2_index++) {
            if (PML2_pointer[PML2_index] & PageFlags::Present) {
              SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\t\tDetected an entry in PML2 table at index: "));
              print_unsigned_number(SystemTable->ConOut, PML2_index);
              SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
              if (PML2_pointer[PML2_index] & PageFlags::Size) {
                SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                  CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\t\tIt's a large page\r\n"));
              } else {
                auto* PML1_pointer = reinterpret_cast<UINT64*>(PML2_pointer[PML2_index] & PML_ENTRY_MASK);
                for (UINTN PML1_index = 0ULL; PML1_index < 512; PML1_index++) {
                  if (PML1_pointer[PML1_index] & PageFlags::Present) {
                    SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                      CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\t\t\tDetected an entry in PML1 table at index: "));
                    print_unsigned_number(SystemTable->ConOut, PML1_index);
                    SystemTable->ConOut->OutputString(SystemTable->ConOut,
                                                      CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
                  }
                }
              }
            }
          }
        }
      }
    }*/
  }
}

void PageTableHierarchy::ZeroOut(UINT8 *start_address, UINTN count) {
  for (UINTN i = 0; i < count; i++) {
    start_address[i] = 0x0;
  }
}

