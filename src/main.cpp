#include "../include/kernel_boot_struct.h"
#include "../include/kernel_loader.h"
#include "../include/utility/PageTableHierarchy.h"

using KernelTrampoline = void(UINT64 entry_point, void* PML4_as_addr, void* boot_info_struct);

extern "C" const char KernelTrampolineStart[];
extern "C" const char KernelTrampolineEnd[];
KernelTrampoline* trampoline;
UINT64 kernel_physical_address = 0x100000;

void AllocateTrampolinesPage(EFI_SYSTEM_TABLE* system_table) {
  constexpr static UINT64 kernel_max_alloc_address = (1ull << X86_64_PHYSICAL_ADDRESS_WIDTH) - 1;
  UINT64 trampoline_address = kernel_max_alloc_address;
  EFI_STATUS status;

  status = system_table->BootServices->AllocatePages(EFI_ALLOCATE_TYPE::AllocateMaxAddress,
                                                     EFI_MEMORY_TYPE::EfiLoaderData,
                                                     1,
                                                     &trampoline_address);
  if (status == EFI_SUCCESS) {
    const auto trampoline_size = KernelTrampolineEnd - KernelTrampolineStart;
    UINT8* src_pointer = reinterpret_cast<UINT8*>(const_cast<char*>(KernelTrampolineStart));
    UINT8* dst_pointer = reinterpret_cast<UINT8*>(trampoline_address);
    for (UINTN i = 0; i < trampoline_size; i++) {
      dst_pointer[i] = src_pointer[i];
    }
    trampoline = reinterpret_cast<KernelTrampoline*>(trampoline_address);
  }
}

[[noreturn]] void JumpToKernel(UINT64 kernel_entry_point, PageTableHierarchy& pth, BootInfoStruct* boot_info_struct) {
  trampoline(kernel_entry_point, pth.get_PML4_ptr_raw(), boot_info_struct);
  for(;;) {

  }
}


void test_runtime_graphics(Frame_Buffer * runtimeGraphics) {
  runtimeGraphics->fill_buffer_box(200, 200, 100, 100, 0xffffffff);
}

void trickled_down_boot(EFI_HANDLE* ImageHandle, EFI_SYSTEM_TABLE* SystemTable) {
  EFI_STATUS status;

  //1. Create Filesystem helper
  Filesystem_Helper filesystem(SystemTable, *ImageHandle);

  //2. Read ELF file
  void* read_buffer{nullptr};
  UINTN elf_file_size = 0UL; //Number of bytes read indicate the size of the file
  UINT64 elf_entry_point = 0UL; //Entry point into the file

  status = read_elf(&filesystem, &read_buffer, &elf_file_size, &elf_entry_point);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Kernel entry point is: "));
  print_unsigned_number(SystemTable->ConOut, elf_entry_point);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));

  if (status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                    L"Failed to read ELF file. Booting will not proceed.\r\n"),
                status);
    return;
  }

  //3. Verify contents of ELF file
  status = verify_elf(SystemTable, read_buffer);
  if (status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                    L"Invalid header. Booting will not proceed \r\n"),
                status);
    return;
  }

  //4. Load contents of ELF file
  PT_LOAD_Descriptor * kernel_pages = nullptr;
  status = load_elf(SystemTable, read_buffer, kernel_physical_address, &kernel_pages);
  if (status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                    L"Invalid header. Booting will not proceed \r\n"),
                status);
    return;
  }

  //5. Create a future page table which will be in use once the UEFI is done.
  PageTableHierarchy pth(SystemTable);
  pth.MapKernelPages(SystemTable, kernel_pages);
  pth.MapEntireRAM(SystemTable);

  //6. Allocate a page for trampoline
  AllocateTrampolinesPage(SystemTable);

  //7. Create runtime graphics protocol
  EFI_GRAPHICS_OUTPUT_PROTOCOL* efi_graphics_output_protocol = nullptr;
  {
    EFI_GUID EFI_GO_GUID_ = create_efi_graphics_output_protocol_GUID();
    status = SystemTable->BootServices->LocateProtocol(&EFI_GO_GUID_, nullptr, reinterpret_cast<void**>(&efi_graphics_output_protocol));
  }

  if (status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                    L"Failed to create EFI Graphics output protocol. Booting will not proceed\r\n"),
                status);
    return;
  }

  BootInfoStruct* boot_info_struct;
  status = SystemTable->BootServices->AllocatePool(EfiLoaderData, sizeof(BootInfoStruct), reinterpret_cast<void**>(&boot_info_struct));
  if (status != EFI_SUCCESS) {
    print_error(SystemTable->ConOut,
                CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(
                    L"Failed to create boot info struct.\r\n"),
                status);
    return;
  }

  boot_info_struct->frame_buffer_ = Create_Runtime_Graphics(efi_graphics_output_protocol);
  boot_info_struct->psf1Font_ = load_PSF1_font(ImageHandle, SystemTable, &filesystem);

  UINT64 bis_as_addr = reinterpret_cast<UINT64>(boot_info_struct);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Address of the boot info struct is: "));
  print_unsigned_hex_number(SystemTable->ConOut, bis_as_addr);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
  UINTN                  MemoryMapSize = 0;
  EFI_MEMORY_DESCRIPTOR* MemoryMap;
  UINTN                  MapKey;
  UINTN                  DescriptorSize;
  UINT32                 DescriptorVersion;

  //The first call will fail on purpose, returns needed memory map size
  SystemTable->BootServices->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion); //NOLINT
  MemoryMapSize += 2 * DescriptorSize;
  SystemTable->BootServices->AllocatePool(EfiLoaderData, MemoryMapSize, reinterpret_cast<void**>(&MemoryMap));
  SystemTable->BootServices->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion);

  // 18.09.2024 - it turns out that OutputString actually allocates. Which invalidates memory map and causes ExitBootServices() to fail.

  /*SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Memory map size: "));
  print_unsigned_number(SystemTable->ConOut, MemoryMapSize);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Memory map key: "));
  print_unsigned_number(SystemTable->ConOut, MapKey);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Descriptor size: "));
  print_unsigned_number(SystemTable->ConOut, DescriptorSize);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));*/

  status  =  SystemTable->BootServices->ExitBootServices(ImageHandle, MapKey);

  if (status == EFI_SUCCESS) {
    //boot_info_struct->page_struct_array_start_ = pth.get_page_struct_array_start();
    //boot_info_struct->psa_size_ = pth.get_psa_size();
    boot_info_struct->memory_map_size_ = MemoryMapSize;
    boot_info_struct->efi_memory_descriptor_ = MemoryMap;
    boot_info_struct->efi_system_table_ = SystemTable;
    boot_info_struct->map_key_ = MapKey;
    boot_info_struct->memory_descriptor_size_ = DescriptorSize;
    JumpToKernel(elf_entry_point, pth, boot_info_struct);
  }
}

EFI_STATUS efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable) {
  SystemTable->ConOut->ClearScreen(SystemTable->ConOut);
  print_current_date(SystemTable);
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"UEFI started.\r\n"));

  UINTN                  MemoryMapSize = 0;
  EFI_MEMORY_DESCRIPTOR* MemoryMap;
  UINTN                  MapKey;
  UINTN                  DescriptorSize;
  UINT32                 DescriptorVersion;

  //The first call will fail on purpose, returns needed memory map size
  SystemTable->BootServices->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion); //NOLINT
  MemoryMapSize += 2 * DescriptorSize;
  SystemTable->BootServices->AllocatePool(EfiLoaderData, MemoryMapSize, reinterpret_cast<void**>(&MemoryMap));
  SystemTable->BootServices->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion);

  UINTN NumberOfEntries = MemoryMapSize / DescriptorSize;

  EFI_GUID EFI_GO_GUID_ = create_efi_graphics_output_protocol_GUID();
  EFI_GRAPHICS_OUTPUT_PROTOCOL* efi_gop_;
  SystemTable->BootServices->LocateProtocol(&EFI_GO_GUID_, nullptr, reinterpret_cast<void**>(&efi_gop_));

  auto runtime_graphics = Create_Runtime_Graphics(efi_gop_);

  CHAR16* prompt = CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"b - boot into kernel\r\ng - runtime graphics test\r\n"
                                                           "m - memory map\r\nr - restart\r\ns- shutdown\r\n");
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, prompt);
  while(1){
    EFI_INPUT_KEY keystroke; //NOLINT
    auto status = SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &keystroke);
    if (status == EFI_SUCCESS) {
      if (Check_Keystroke_Value(keystroke, 'b')) {
        //BOOT CODE here
        trickled_down_boot(&ImageHandle, SystemTable);
        return 0;
      }
      else if (Check_Keystroke_Value(keystroke, 'm')) {
        //Memory map print here
        output_EFI_memory_status(SystemTable->ConOut, MemoryMap, NumberOfEntries, DescriptorSize);
        while(1) {
          auto status2 = SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &keystroke);
          if (status2 == EFI_SUCCESS) {
            break;
          }
        }
      }
      else if (Check_Keystroke_Value(keystroke, 'g')) {
        //Test runtime graphics buffer
        test_runtime_graphics(&runtime_graphics);
        while(1) {
          auto status2 = SystemTable->ConIn->ReadKeyStroke(SystemTable->ConIn, &keystroke);
          if (status2 == EFI_SUCCESS) {
            break;
          }
        }
      }
      else if (Check_Keystroke_Value(keystroke, 'r')) {
        SystemTable->RuntimeServices->ResetSystem(EfiResetWarm, EFI_SUCCESS, 0, nullptr);
      }
      else if (Check_Keystroke_Value(keystroke, 's')) {
        break;
      }
      SystemTable->ConOut->ClearScreen(SystemTable->ConOut);
      SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);
      SystemTable->ConOut->OutputString(SystemTable->ConOut, prompt);
    }
  }
  SystemTable->RuntimeServices->ResetSystem(EfiResetShutdown, EFI_SUCCESS, 0, nullptr);
  return 0;
}
