#!/bin/bash
BUILD_PATH=../build
if [[ ! -d $BUILD_PATH ]]; then
    echo "Did not find build directory in current project. Creating one..."
    mkdir $BUILD_PATH
fi
cd $BUILD_PATH
cmake .. -D CMAKE_CXX_COMPILER=/usr/bin/clang++
make
rm -rf CMakeFiles
rm cmake_install.cmake
rm CMakeCache.txt
rm Makefile