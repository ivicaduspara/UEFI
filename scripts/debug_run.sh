#!/bin/bash
if [[ $# != 1 && $# != 2 ]]; then
  echo "Invalid number of arguments. Provide 1 argument - path to OVMF.fd file or 2 arguments - path to OVMF.fd file and path to .img file."
else
  OVMF_FILE=""
  UEFI_IMG=""
  if [[ $# == 1 ]]; then
    echo "Single argument provided. Assuming $1 is path to OVMF.fd file. Looking for image file in ./disk/uefi.img"
    OVMF_FILE=$1
    UEFI_IMG=../disk/uefi.img
  else
    echo "Two arguments provided. Using $1 as path to OVMF.fd file and $2 as path to image file."
    OVMF_FILE=$1
    UEFI_IMG=$2
  fi
  if [[ ! -f $UEFI_IMG ]]; then
    echo "File $UEFI_IMG does not exist."
  elif [[ ! -f $OVMF_FILE ]]; then
    echo "File $OVMF_FILE does not exist"
  else
    qemu-system-x86_64 -s -S --machine q35 -m 512 -cpu Skylake-Client-v3 -bios $OVMF_FILE -drive file=$UEFI_IMG,if=ide -d cpu_reset
  fi
fi