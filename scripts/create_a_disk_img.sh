#!/bin/bash
if [[ $# != 0 && $# != 1 ]]; then
  echo "Create a disk script takes 1(optional) argument - Path to the location where disk is created."
else
  DIR=""
  IMG=""
  if [[ $# == 0 ]]; then
    echo "No arguments provided. Creating ../disk/uefi.img"
    DIR=../disk
    IMG=../disk/uefi.img
    if [[ ! -d $DIR ]]; then
      echo "Did not find <project_root>/disk directory. Creating one..."
      mkdir $DIR
    fi
  else
    IMG=$1
    echo "Creating a disk image file at $IMG"
  fi
  if [[ -d $IMG ]]; then
    echo "You can't provide a path to a directory. Path has to include desired file name."
  else
    dd if=/dev/zero of=$IMG bs=512 count=93750
    parted $IMG -s -a minimal mklabel gpt
    parted $IMG -s -a minimal mkpart EFI FAT16 2048s 93716s
    parted $IMG -s -a minimal toggle 1 boot
  fi
fi
