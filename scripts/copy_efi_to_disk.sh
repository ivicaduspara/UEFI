#!/bin/bash
if [[ $# != 0 && $# != 2 ]]; then
  echo "Invalid number of arguments. Provide 0 arguments for default behavior, or 2 arguments - path to a disk image and path to .EFI file."
else
  IMG=""
  EFI_FILE=""
  if [[ $# == 0 ]]; then
    echo "No arguments provided. Checking for 'uefi.img' in ../disk/ and 'BOOTX64.EFI'. in ../build."
    IMG=../disk/uefi.img
    EFI_FILE=../build/BOOTX64.EFI
  else
    echo "Using $1 as disk image path and $2 as .EFI file path"
    IMG=$1
    EFI_FILE=$2
  fi
  if [[ ! -f $IMG ]]; then
      echo "Could not find image file at $IMG"
  elif [[ ! -f $EFI_FILE ]]; then
      echo "Could not find .EFI file at $EFI_FILE"
  else
    dd if=/dev/zero of=./tmp.img bs=512 count=91669
    mformat -i ./tmp.img -h 32 -t 32 -n 64 -c 1
    mmd -i ./tmp.img ::/EFI
    mmd -i ./tmp.img ::/EFI/BOOT
    mcopy -i ./tmp.img $EFI_FILE ::/EFI/BOOT
    dd if=./tmp.img of=$IMG bs=512 count=91669 seek=2048 conv=notrunc
    rm ./tmp.img
  fi
fi