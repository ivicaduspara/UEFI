#!/bin/bash
if [[ $# != 0 && $# != 3 ]]; then
  echo "Invalid number of arguments. Provide 0 arguments for default behavior,
        or 3 arguments - path to a disk image, path to .EFI file, path to kernel.elf file."
else
  IMG=""
  EFI_FILE=""
  KERNEL_FILE=""
  if [[ $# == 0 ]]; then
    echo "No arguments provided. Checking for 'uefi.img' in ../disk/, 'BOOTX64.EFI'. in ../build and 'kernel.elf' in ../build"
    IMG=../disk/uefi.img
    EFI_FILE=../build/BOOTX64.EFI
    KERNEL_FILE=../build/kernel.elf
  else
    echo "Using $1 as disk image name, $2 as .EFI file name and $3 as kernel.elf"
    IMG=$1
    EFI_FILE=$2
    KERNEL_FILE=$3
  fi
  if [[ ! -f $IMG ]]; then
    echo "Could not find image file at $IMG"
  elif [[ ! -f $EFI_FILE ]]; then
    echo "Could not find .EFI file at $EFI_FILE"
  elif [[ ! -f $KERNEL_FILE ]]; then
    echo "Could not find kernel.elf file at $KERNEL_FILE"
  else
    dd if=/dev/zero of=./tmp.img bs=512 count=91669
    mformat -i ./tmp.img -h 32 -t 32 -n 64 -c 1
    mmd -i ./tmp.img ::/EFI
    mmd -i ./tmp.img ::/EFI/BOOT
    mcopy -i ./tmp.img $EFI_FILE ::/EFI/BOOT
    mcopy -i ./tmp.img $KERNEL_FILE ::
    dd if=./tmp.img of=$IMG bs=512 count=91669 seek=2048 conv=notrunc
    rm ./tmp.img
  fi
fi