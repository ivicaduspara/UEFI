#ifndef UEFI_IMPL_LOADED_IMAGE_PROTOCOL_H
#define UEFI_IMPL_LOADED_IMAGE_PROTOCOL_H
//UEFI 2.9 specs, page 288
#include "../system-table/efi_system_table.h"

#define EFI_LOADED_IMAGE_PROTOCOL_GUID {0x5B1B31A1,0x9562,0x11d2,{0x8E,0x3F,0x00,0xA0,0xC9,0x69,0x72,0x3B}}
#define EFI_LOADED_IMAGE_DEVICE_PATH_PROTOCOL_GUID {0xbc62157e,0x3e33,0x4fec,{0x99,0x20,0x2d,0x3b,0x36,0xd7,0x50,0xdf}}

EFI_GUID create_efi_loaded_image_protocol_GUID() {
  return EFI_GUID{0x5B1B31A1,0x9562,0x11d2,{0x8E,0x3F,0x00,0xA0,0xC9,0x69,0x72,0x3B}};
}

EFI_GUID create_efi_loaded_image_device_path_protocol_GUID() {
  return EFI_GUID{0xbc62157e,0x3e33,0x4fec,{0x99,0x20,0x2d,0x3b,0x36,0xd7,0x50,0xdf}};
}

using EFI_IMAGE_UNLOAD = EFI_STATUS (*)(EFI_HANDLE ImageHandle);

struct EFI_LOADED_IMAGE_PROTOCOL { //NOLINT
  UINT32                        Revision;
  EFI_HANDLE                    ParentHandle;
  EFI_SYSTEM_TABLE*             SystemTable;

// Source location of the image
  EFI_HANDLE                    DeviceHandle;
  EFI_DEVICE_PATH_PROTOCOL*     FilePath;
  void*                         Reserved;

// Image’s load options
  UINT32                        LoadOptionsSize;
  void*                         LoadOptions;

  void*                         ImageBase;
  UINT64                        ImageSize;
  EFI_MEMORY_TYPE               ImageCodeType;
  EFI_MEMORY_TYPE               ImageDataType;
  EFI_IMAGE_UNLOAD              Unload;
};
#endif //UEFI_IMPL_LOADED_IMAGE_PROTOCOL_H
