#ifndef UEFI_IMPL_EFI_TIME_SERVICES_H
#define UEFI_IMPL_EFI_TIME_SERVICES_H
#include "efi_types.h"

//UEFI 2.9 specs, page 258

//*******************************************************
// Bit Definitions for EFI_TIME.Daylight. See below.
//*******************************************************
#define EFI_TIME_ADJUST_DAYLIGHT 0x01
#define EFI_TIME_IN_DAYLIGHT 0x02


//*******************************************************
// Value Definition for EFI_TIME.TimeZone. See below.
//*******************************************************
#define EFI_UNSPECIFIED_TIMEZONE 0x07FF

struct EFI_TIME { //NOLINT
  UINT16      Year; // 1900 – 9999
  UINT8       Month; // 1 – 12
  UINT8       Day; // 1 – 31
  UINT8       Hour; // 0 – 23
  UINT8       Minute; // 0 – 59
  UINT8       Second; // 0 – 59
  UINT8       Pad1;
  UINT32      Nanosecond; // 0 – 999,999,999
  INT16       TimeZone; // -1440 to 1440 or 2047
  UINT8       Daylight;
  UINT8       Pad2;
};

struct  EFI_TIME_CAPABILITIES {
  UINT32 Resolution;
  UINT32 Accuracy;
  BOOLEAN SetsToZero;
};

using GetTime = EFI_STATUS (*)(EFI_TIME* Time, EFI_TIME_CAPABILITIES* Capabilities);
using SetTime = EFI_STATUS (*)(EFI_TIME* Time);
using GetWakeupTime = EFI_STATUS (*)(BOOLEAN* Enabled, BOOLEAN* Pending, EFI_TIME* Time);
using SetWakeupTime = EFI_STATUS (*)(BOOLEAN Enable, EFI_TIME* Time);
#endif //UEFI_IMPL_EFI_TIME_SERVICES_H
