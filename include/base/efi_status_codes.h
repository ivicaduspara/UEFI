#ifndef UEFI_IMPL_EFI_STATUS_CODES_H
#define UEFI_IMPL_EFI_STATUS_CODES_H
#include "efi_types.h"

#define EFI_SUCCESS                             0x0000000000000000
#define EFI_LOAD_ERROR                          0x8000000000000001
#define EFI_INVALID_PARAMETER                   0x8000000000000002
#define EFI_UNSUPPORTED                         0x8000000000000003
#define EFI_BAD_BUFFER_SIZE                     0x8000000000000004
#define EFI_BUFFER_TOO_SMALL                    0x8000000000000005
#define EFI_NOT_READY                           0x8000000000000006
#define EFI_DEVICE_ERROR                        0X8000000000000007
#define EFI_WRITE_PROTECTED                     0X8000000000000008
#define EFI_OUT_OF_RESOURCES                    0X8000000000000009
#define EFI_VOLUME_CORRUPTED                    0X800000000000000A
#define EFI_VOLUME_FULL                         0X800000000000000B
#define EFI_NO_MEDIA                            0X800000000000000C
#define EFI_MEDIA_CHANGED                       0X800000000000000D
#define EFI_NOT_FOUND                           0X800000000000000E
#define EFI_ACCESS_DENIED                       0X800000000000000F
#define EFI_NO_RESPONSE                         0x8000000000000010
#define EFI_NO_MAPPING                          0x8000000000000011
#define EFI_TIMEOUT                             0x8000000000000012
#define EFI_NOT_STARTED                         0x8000000000000013
#define EFI_ALREADY_STARTED                     0x8000000000000014
#define EFI_ABORTED                             0x8000000000000015
#define EFI_ICMP_ERROR                          0x8000000000000016
#define EFI_TFTP_ERROR                          0x8000000000000017
#define EFI_PROTOCOL_ERROR                      0x8000000000000018
#define EFI_INCOMPATIBLE_VERSION                0x8000000000000019
#define EFI_SECURITY_VIOLATION                  0x800000000000001A
#define EFI_CRC_ERROR                           0x800000000000001B
#define EFI_END_OF_MEDIA                        0x800000000000001C
#define EFI_END_OF_FILE                         0x800000000000001D
#define EFI_INVALID_LANGUAGE                    0x800000000000001E
#define EFI_COMPROMISED_DATA                    0x800000000000001F
#define EFI_IP_ADDRESS_CONFLICT                 0x8000000000000020
#define EFI_HTTP_ERROR                          0x8000000000000021

#define EFI_WARN_UNKNOWN_GLYPH                  0x0000000000000001
#define EFI_WARN_DELETE_FAILURE                 0x0000000000000002
#define EFI_WARN_WRITE_FAILURE                  0x0000000000000003
#define EFI_WARN_BUFFER_TOO_SMALL               0x0000000000000004
#define EFI_WARN_STALE_DATA                     0x0000000000000005
#define EFI_WARN_FILE_SYSTEM                    0x0000000000000006
#define EFI_WARN_RESET_REQUIRED                 0x0000000000000007

// Adding this for convenience
inline CHAR16* CheckStandardEFIError(UINT64 s) {
  switch(s) {
    case EFI_LOAD_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Load Error");
    }
    case EFI_INVALID_PARAMETER: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Invalid Parameter");
    }
    case EFI_UNSUPPORTED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Unsupported");
    }
    case EFI_BAD_BUFFER_SIZE: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Bad Buffer Size");
    }
    case EFI_BUFFER_TOO_SMALL: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Buffer Too Small");
    }
    case EFI_NOT_READY: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Not Ready");
    }
    case EFI_DEVICE_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Device Error");
    }
    case EFI_WRITE_PROTECTED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Write Protected");
    }
    case EFI_OUT_OF_RESOURCES: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Out Of Resources");
    }
    case EFI_VOLUME_CORRUPTED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Volume Corrupted");
    }
    case EFI_VOLUME_FULL: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Volume Full");
    }
    case EFI_NO_MEDIA: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" No Media");
    }
    case EFI_MEDIA_CHANGED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Media Changed");
    }
    case EFI_NOT_FOUND: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" File Not Found");
    }
    case EFI_ACCESS_DENIED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Access Denied");
    }
    case EFI_NO_RESPONSE: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" No Response");
    }
    case EFI_NO_MAPPING: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" No Mapping");
    }
    case EFI_TIMEOUT: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Timeout");
    }
    case EFI_NOT_STARTED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Not Started");
    }
    case EFI_ALREADY_STARTED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Already Started");
    }
    case EFI_ABORTED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Aborted");
    }
    case EFI_ICMP_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" ICMP Error");
    }
    case EFI_TFTP_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" TFTP Error");
    }
    case EFI_PROTOCOL_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Protocol Error");
    }
    case EFI_INCOMPATIBLE_VERSION: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Incompatible Version");
    }
    case EFI_SECURITY_VIOLATION: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Security Violation");
    }
    case EFI_CRC_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" CRC Error");
    }
    case EFI_END_OF_MEDIA: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" End Of Media");
    }
    case EFI_END_OF_FILE: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" End Of File");
    }
    case EFI_INVALID_LANGUAGE: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Invalid Language");
    }
    case EFI_COMPROMISED_DATA: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Compromised Data");
    }
    case EFI_IP_ADDRESS_CONFLICT: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" IP Address Conflict");
    }
    case EFI_HTTP_ERROR: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" End Of File");
    }
    case EFI_WARN_UNKNOWN_GLYPH: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - Unknown Glyph");
    }
    case EFI_WARN_DELETE_FAILURE: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - Delete Failure");
    }
    case EFI_WARN_WRITE_FAILURE: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - Write Failure");
    }
    case EFI_WARN_BUFFER_TOO_SMALL: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - Buffer Too Small");
    }
    case EFI_WARN_STALE_DATA: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - Stale Data");
    }
    case EFI_WARN_FILE_SYSTEM: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - File System");
    }
    case EFI_WARN_RESET_REQUIRED: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" WARNING - Reset Required");
    }
    case EFI_SUCCESS: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" Successful");
    }
    default: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" ERROR - Could not match code");
    }
  }
}

#endif //UEFI_IMPL_EFI_STATUS_CODES_H
