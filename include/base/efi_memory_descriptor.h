#ifndef UEFI_IMPL_EFI_MEMORY_DESCRIPTOR_H
#define UEFI_IMPL_EFI_MEMORY_DESCRIPTOR_H
#include "efi_types.h"


#define EFI_MEMORY_DESCRIPTOR_VERSION 1

//UEFI 2.9 spec, page 171
//*******************************************************
// Memory Attribute Definitions
//*******************************************************
// These types can be “ORed” together as needed.
#define EFI_MEMORY_UC                     0x0000000000000001
#define EFI_MEMORY_WC                     0x0000000000000002
#define EFI_MEMORY_WT                     0x0000000000000004
#define EFI_MEMORY_WB                     0x0000000000000008
#define EFI_MEMORY_UCE                    0x0000000000000010
#define EFI_MEMORY_WP                     0x0000000000001000
#define EFI_MEMORY_RP                     0x0000000000002000
#define EFI_MEMORY_XP                     0x0000000000004000
#define EFI_MEMORY_NV                     0x0000000000008000
#define EFI_MEMORY_MORE_RELIABLE          0x0000000000010000
#define EFI_MEMORY_RO                     0x0000000000020000
#define EFI_MEMORY_SP                     0x0000000000040000
#define EFI_MEMORY_CPU_CRYPTO             0x0000000000080000
#define EFI_MEMORY_RUNTIME                0x8000000000000000

//UEFI 2.9 spec, page 172
using EFI_PHYSICAL_ADDRESS = UINT64;
using EFI_VIRTUAL_ADDRESS = UINT64;

enum EFI_ALLOCATE_TYPE {
  AllocateAnyPages,
  AllocateMaxAddress,
  AllocateAddress,
  MaxAllocateType
};

enum EFI_MEMORY_TYPE {
  EfiReservedMemoryType,
  EfiLoaderCode,
  EfiLoaderData,
  EfiBootServicesCode,
  EfiBootServicesData,
  EfiRuntimeServicesCode,
  EfiRuntimeServicesData,
  EfiConventionalMemory,
  EfiUnusableMemory,
  EfiACPIReclaimMemory,
  EfiACPIMemoryNVS,
  EfiMemoryMappedIO,
  EfiMemoryMappedIOPortSpace,
  EfiPalCode,
  EfiPersistentMemory,
  EfiUnacceptedMemoryType,
  EfiMaxMemoryType
};

inline CHAR16* Get_Name_Of_EFI_MEMORY_TYPE(EFI_MEMORY_TYPE memory_type) {
  switch(memory_type) {
    case EFI_MEMORY_TYPE::EfiReservedMemoryType: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiReservedMemoryType");
    }
    case EFI_MEMORY_TYPE::EfiLoaderCode: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiLoaderCode");
    }
    case EFI_MEMORY_TYPE::EfiLoaderData: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiLoaderData");
    }
    case EFI_MEMORY_TYPE::EfiBootServicesCode: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiBootServicesCode");
    }
    case EFI_MEMORY_TYPE::EfiBootServicesData: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiBootServicesData");
    }
    case EFI_MEMORY_TYPE::EfiRuntimeServicesCode: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiRuntimeServicesCode");
    }
    case EFI_MEMORY_TYPE::EfiRuntimeServicesData: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiRuntimeServicesData");
    }
    case EFI_MEMORY_TYPE::EfiConventionalMemory: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiConventionalMemory");
    }
    case EFI_MEMORY_TYPE::EfiUnusableMemory: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiUnusableMemory");
    }
    case EFI_MEMORY_TYPE::EfiACPIReclaimMemory: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiACPIReclaimMemory");
    }
    case EFI_MEMORY_TYPE::EfiACPIMemoryNVS: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiACPIMemoryNVS");
    }
    case EFI_MEMORY_TYPE::EfiMemoryMappedIO: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiMemoryMappedIO");
    }
    case EFI_MEMORY_TYPE::EfiMemoryMappedIOPortSpace: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiMemoryMappedIOPortSpace");
    }
    case EFI_MEMORY_TYPE::EfiPalCode: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiPalCode");
    }
    case EFI_MEMORY_TYPE::EfiPersistentMemory: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiPersistentMemory");
    }
    case EFI_MEMORY_TYPE::EfiUnacceptedMemoryType: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiUnacceptedMemoryType");
    }
    case EFI_MEMORY_TYPE::EfiMaxMemoryType: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiMaxMemoryType");
    }
    default: {
      return CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Unknown EFI memory type");
    }
  }
}

inline CHAR16* EFI_MEMORY_TYPE_Strings[16] = {
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiReservedMemoryType"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiLoaderCode"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiLoaderData"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiBootServicesCode"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiBootServicesData"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiRuntimeServicesCode"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiRuntimeServicesData"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiConventionalMemory"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiUnusableMemory"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiACPIReclaimMemory"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiACPIMemoryNVS"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiMemoryMappedIO"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiMemoryMappedIOPortSpace"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiPalCode"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiPersistentMemory"),
    CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"EfiMaxMemoryType")
};

//UEFI 2.9, page 170
struct  EFI_MEMORY_DESCRIPTOR { //NOLINT
  UINT32                    Type;
  EFI_PHYSICAL_ADDRESS      PhysicalStart;
  EFI_VIRTUAL_ADDRESS       VirtualStart;
  UINT64                    NumberOfPages;
  UINT64                    Attribute;
};

#endif //UEFI_IMPL_EFI_MEMORY_DESCRIPTOR_H
