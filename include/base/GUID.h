#ifndef UEFI_IMPL_GUID_H
#define UEFI_IMPL_GUID_H
#include "efi_types.h"

//UEFI 2.9 spec, page 181
struct EFI_GUID {
  UINT32      Data1;
  UINT16      Data2;
  UINT16      Data3;
  UINT8       Data4[8];
};

inline bool operator==(const EFI_GUID& g1, const EFI_GUID& g2) noexcept {
  bool data1_match = g1.Data1 == g2.Data1;
  bool data2_match = g1.Data2 == g2.Data2;
  bool data3_match = g1.Data3 == g2.Data3;
  bool data4_match = true;
  for (int i = 0; i < 8; i++) {
    data4_match = data4_match && (g1.Data4[i] == g2.Data4[i]);
  }
  return data1_match && data2_match && data3_match && data4_match;
}
#endif //UEFI_IMPL_GUID_H
