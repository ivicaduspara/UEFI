#ifndef UEFI_IMPL_EFI_TABLE_HEADER_H
#define UEFI_IMPL_EFI_TABLE_HEADER_H
#include "efi_types.h"

//UEFI 2.9 specs, page 92.
struct EFI_TABLE_HEADER { //NOLINT
  UINT64      Signature;
  UINT32      Revision;
  UINT32      HeaderSize;
  UINT32      CRC32;
  UINT32      Reserved;
};
#endif //UEFI_IMPL_EFI_TABLE_HEADER_H
