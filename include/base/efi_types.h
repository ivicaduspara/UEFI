#ifndef UEFI_IMPL_EFI_TYPES_H
#define UEFI_IMPL_EFI_TYPES_H
/* Contains alias definitions for EFI implementation.
 * stdint.h is used because cstdint doesn't work with Clang++.
 * */
#include <stdint.h> //NOLINT

#define EFI_BLACK                               0x00
#define EFI_BLUE                                0x01
#define EFI_GREEN                               0x02
#define EFI_CYAN                                0x03
#define EFI_RED                                 0x04
#define EFI_MAGENTA                             0x05
#define EFI_BROWN                               0x06
#define EFI_LIGHTGRAY                           0x07
#define EFI_DARKGRAY                            0x08
#define EFI_LIGHTBLUE                           0x09
#define EFI_LIGHTGREEN                          0x0A
#define EFI_LIGHTCYAN                           0x0B
#define EFI_LIGHTRED                            0x0C
#define EFI_LIGHTMAGENTA                        0x0D
#define EFI_YELLOW                              0x0E
#define EFI_WHITE                               0x0F

#define EFI_BACKGROUND_BLACK                    0x00
#define EFI_BACKGROUND_BLUE                     0x10
#define EFI_BACKGROUND_GREEN                    0x20
#define EFI_BACKGROUND_CYAN                     0x30
#define EFI_BACKGROUND_RED                      0x40
#define EFI_BACKGROUND_MAGENTA                  0x50
#define EFI_BACKGROUND_BROWN                    0x60
#define EFI_BACKGROUND_LIGHTGRAY                0x70

#define CAST_CPP_CHAR_STAR_TO_UEFI_CHAR16_STAR(X) \
(reinterpret_cast<CHAR16*>(const_cast<char_t*>(X)))

#define CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(X) \
(reinterpret_cast<CHAR16*>(const_cast<wchar_t*>(X)))

using CHAR16 = uint_least16_t;
using INT8 = int8_t;
using UINT8 = uint8_t;
using INT16 = int16_t;
using UINT16 = uint16_t;
using INT32 = int32_t;
using UINT32 = uint32_t;
using INT64 = int64_t;
using UINT64 = uint64_t;
using UINTN = unsigned long long;
using BOOLEAN = uint8_t;
using EFI_HANDLE = void*;
using EFI_STATUS = UINT64;
using SIZE_T = size_t;
using EFI_EVENT = void*;
#endif //UEFI_IMPL_EFI_TYPES_H
