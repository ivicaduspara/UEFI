#ifndef UEFI_IMPL_KERNEL_LOADER_H
#define UEFI_IMPL_KERNEL_LOADER_H
#include "efi.h"
#include "elf.h"
#include "utility/PT_LOAD_Descriptor.h"
#include "utility/utility.h"
#include "utility/PageFlags.h"

EFI_STATUS read_elf(Filesystem_Helper* filesystem, void** file_buffer, UINTN* number_of_bytes_read, UINT64* entry_point) {
  *number_of_bytes_read = 0UL;
  EFI_FILE_PROTOCOL* elf_file_handle = nullptr;
  elf_file_handle = filesystem->open_file(CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"kernel.elf"), EFI_FILE_MODE_READ);
  if (!elf_file_handle) {
    return EFI_LOAD_ERROR;
  }
  auto status = filesystem->read_file(elf_file_handle, file_buffer, number_of_bytes_read);
  if (status != EFI_SUCCESS) {
    *number_of_bytes_read = 0UL;
    *entry_point = 0UL;
  }
  filesystem->close(elf_file_handle);
  Elf64_Ehdr* elf_header = reinterpret_cast<Elf64_Ehdr*>(*file_buffer);
  *entry_point = elf_header->e_entry;
  return status;
}

EFI_STATUS verify_elf(EFI_SYSTEM_TABLE* SystemTable, void* file_buffer) {
  Elf64_Ehdr* elf_file_header = reinterpret_cast<Elf64_Ehdr*>(file_buffer);
  if (memcmp(&elf_file_header->e_ident[EI_MAG0], ELFMAG, SELFMAG, SystemTable) != 0) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Magic bytes for Elf64 did not match.\r\n"));
    return EFI_LOAD_ERROR;
  }
  if (elf_file_header->e_ident[EI_CLASS] != ELFCLASS64) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Invalid ELF class. Expected 64-bit ELF file.\r\n"));
    return EFI_LOAD_ERROR;
  }
  if (elf_file_header->e_ident[EI_DATA] != ELFDATA2LSB) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Invalid endianness. Expected little endian file.\r\n"));
    return EFI_LOAD_ERROR;
  }
  if(elf_file_header->e_version != EV_CURRENT) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"ELF version is not current. Version field in header must be 1.\r\n"));
    return EFI_LOAD_ERROR;
  }
  if(elf_file_header->e_machine != EM_X86_64) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Provided ELF file is not compiled for x86-64 architecture.\r\n"));
    return EFI_LOAD_ERROR;
  }
  if(elf_file_header->e_type != ET_EXEC) {
    SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Provided ELF file is not executable.\r\n"));
    return EFI_LOAD_ERROR;
  }
  return EFI_SUCCESS;
}

EFI_STATUS read_program_headers_information(void* file_buffer,
                                            UINT64* start_offset_of_program_headers,
                                            UINT16* number_of_program_headers,
                                            UINT16* size_of_single_program_header) {
  Elf64_Ehdr* elf_header = reinterpret_cast<Elf64_Ehdr*>(file_buffer);
  *start_offset_of_program_headers = elf_header->e_phoff;
  *number_of_program_headers = elf_header->e_phnum;
  *size_of_single_program_header = elf_header->e_phentsize;
  return EFI_SUCCESS;
}

/* When loading an ELF file, the segments which have to be loaded are ones of type "PT_LOAD".
 * Every segment has a specified alignment. These alignments are not included
 * in `p_filesz` and `p_memsz` fields. They have to be aligned up before allocating memory.
 *
 * `p_filesz` is the size of segment inside ELF file. That means that `p_filesz` have to be copied into allocated memory.
 * `p_memsz` is the size of segment in memory.
 *
 *
 * */
EFI_STATUS load_elf(EFI_SYSTEM_TABLE* SystemTable, void* file_buffer, UINT64 kernel_physical_address,
                    PT_LOAD_Descriptor** kernel_pages) {

  UINT64 phdr_offset = 0UL;
  UINT16 num_of_phdrs = 0x0000;
  UINT16 phdr_size = 0x0000;
  BOOLEAN first = 0;
  PT_LOAD_Descriptor* descriptor = nullptr;

  read_program_headers_information(file_buffer, &phdr_offset, &num_of_phdrs, &phdr_size);
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"No of program headers: "));
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_GREEN);
  print_unsigned_number(SystemTable->ConOut, num_of_phdrs);
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" , Single program header size: "));
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_GREEN);
  print_unsigned_number(SystemTable->ConOut, phdr_size);
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L" , Program header offset: "));
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_GREEN);
  print_unsigned_number(SystemTable->ConOut, phdr_offset);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_WHITE);

  //First header.
  Elf64_Phdr*program_header = reinterpret_cast<Elf64_Phdr*>(reinterpret_cast<UINT8*>(file_buffer) + phdr_offset);
  for(UINT16 index = 0; index < num_of_phdrs; index++) {
    switch(program_header->p_type) {
      case PT_LOAD: {
        UINTN memory_size_aligned_up = (program_header->p_memsz + program_header->p_align - 1) & ~(program_header->p_align - 1);
        UINTN number_of_pages = memory_size_aligned_up / 0x1000;
        SystemTable->BootServices->AllocatePages(AllocateAddress, EfiLoaderData, number_of_pages, &kernel_physical_address);
        if (first == 0) {
          SystemTable->BootServices->AllocatePool(EfiBootServicesData, sizeof(PT_LOAD_Descriptor), reinterpret_cast<void**>(&descriptor));
          descriptor->physical_address = kernel_physical_address;
          descriptor->virtual_address = program_header->p_vaddr;
          descriptor->number_pages = number_of_pages;
          //p_flags: 1 = executable, 2 = writeable, 4 = readable
          UINT64 flags = 0ULL;
          if (program_header->p_flags & 0x1ULL) {
            flags = PageFlags::Present;
          } else if (program_header->p_flags & 0x2ULL) {
            flags = PageFlags::Kernel_RW;
          } else {
            flags = PageFlags::Kernel_RO;
          }
          descriptor->flags = flags;
          *kernel_pages = descriptor;
          (*kernel_pages)->next_descriptor = nullptr;
          first = 1;
        } else {
          SystemTable->BootServices->AllocatePool(EfiBootServicesData, sizeof(PT_LOAD_Descriptor), reinterpret_cast<void**>(&descriptor->next_descriptor));
          descriptor->next_descriptor->physical_address = kernel_physical_address;
          descriptor->next_descriptor->virtual_address = program_header->p_vaddr;
          descriptor->next_descriptor->number_pages = number_of_pages;
          descriptor->next_descriptor->flags = program_header->p_flags;
          descriptor->next_descriptor->next_descriptor = nullptr;
          descriptor = descriptor->next_descriptor;
        }
        //w_buffer is the write buffer of current PT_LOAD segment (first byte of allocated page(s)).

        //Zero out entire allocated memory first.
        UINT8* w_buffer = reinterpret_cast<UINT8*>(kernel_physical_address);
        for(UINTN i = 0; i < memory_size_aligned_up; i++) {
          w_buffer[i] = 0x00;
        }

        //Now copy p_filesz bytes into the write buffer
        UINT8* r_buffer = reinterpret_cast<UINT8*>(file_buffer) + program_header->p_offset;
        for(UINTN i = 0; i < program_header->p_filesz; i++) {
          w_buffer[i] = r_buffer[i];
        }
        kernel_physical_address += memory_size_aligned_up;
        break;
      }
    }
    program_header = reinterpret_cast<Elf64_Phdr*>(reinterpret_cast<UINT8*>(program_header) + phdr_size);
  }

  return EFI_SUCCESS;
}

#endif // UEFI_IMPL_KERNEL_LOADER_H
