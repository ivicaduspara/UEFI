#ifndef UEFI_IMPL_EFI_PIXEL_BITMASK_H
#define UEFI_IMPL_EFI_PIXEL_BITMASK_H
#include "../base/efi_types.h"

struct EFI_PIXEL_BITMASK {
  UINT32        RedMask;
  UINT32        GreenMask;
  UINT32        BlueMask;
  UINT32        ReservedMask;
};
#endif //UEFI_IMPL_EFI_PIXEL_BITMASK_H
