#ifndef UEFI_IMPL_GRAPHICS_PROTOCOLS_H
#define UEFI_IMPL_GRAPHICS_PROTOCOLS_H
#include "../base/GUID.h"
#include "efi_pixel_bitmask.h"

#define EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID \
{0x9042a9de,0x23dc,0x4a38,\
{0x96,0xfb,0x7a,0xde,0xd0,0x80,0x51,0x6a}}

EFI_GUID create_efi_graphics_output_protocol_GUID() {
  return EFI_GUID {0x9042a9de, 0x23dc, 0x4a38, {0x96,0xfb,0x7a,0xde,0xd0,0x80,0x51,0x6a}};
};

//UEFI 2.9 specs, page 491

enum EFI_GRAPHICS_OUTPUT_BLT_OPERATION {
  EfiBltVideoFill,
  EfiBltVideoToBltBuffer,
  EfiBltBufferToVideo,
  EfiBltVideoToVideo,
  EfiGraphicsOutputBltOperationMax
};

struct EFI_GRAPHICS_OUTPUT_BLT_PIXEL {
  UINT8       Reserved;
  UINT8       Red;
  UINT8       Blue;
  UINT8       Green;
};

enum EFI_GRAPHICS_PIXEL_FORMAT {
  PixelRedGreenBlueReserved8BitPerColor,
  PixelBlueGreenRedReserved8BitPerColor,
  PixelBitMask,
  PixelBltOnly,
  PixelFormatMax
};

struct EFI_GRAPHICS_OUTPUT_MODE_INFORMATION {
  UINT32                      Version;
  UINT32                      HorizontalResolution;
  UINT32                      VerticalResolution;
  EFI_GRAPHICS_PIXEL_FORMAT   PixelFormat;
  EFI_PIXEL_BITMASK           PixelInformation;
  UINT32                      PixelsPerScanLine;
};

struct EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE { //NOLINT
  UINT32                                    MaxMode;
  UINT32                                    Mode;
  EFI_GRAPHICS_OUTPUT_MODE_INFORMATION*     Info;
  UINTN                                     SizeOfInfo;
  EFI_PHYSICAL_ADDRESS                      FrameBufferBase;
  UINTN                                     FrameBufferSize;
};

struct EFI_GRAPHICS_OUTPUT_PROTOCOL;

using EFI_GRAPHICS_OUTPUT_PROTOCOL_QUERY_MODE = EFI_STATUS(*)(EFI_GRAPHICS_OUTPUT_PROTOCOL* This, UINT32 ModeNumber,
                                                               UINTN *SizeOfInfo, EFI_GRAPHICS_OUTPUT_MODE_INFORMATION** Info);
using EFI_GRAPHICS_OUTPUT_PROTOCOL_SET_MODE = EFI_STATUS(*)(EFI_GRAPHICS_OUTPUT_PROTOCOL* This, UINT32 ModeNumber);
using EFI_GRAPHICS_OUTPUT_PROTOCOL_BLT = EFI_STATUS(*)(EFI_GRAPHICS_OUTPUT_PROTOCOL* This, EFI_GRAPHICS_OUTPUT_BLT_PIXEL* BltBuffer,
                                                       EFI_GRAPHICS_OUTPUT_BLT_OPERATION BltOperation, UINTN SourceX,
                                                       UINTN SourceY, UINTN DestinationX, UINTN DestinationY,
                                                       UINTN Width, UINTN Height, UINTN Delta);

// UEFI 2.9 spec, page 485
struct EFI_GRAPHICS_OUTPUT_PROTOCOL { //NOLINT
  EFI_GRAPHICS_OUTPUT_PROTOCOL_QUERY_MODE  QueryMode;
  EFI_GRAPHICS_OUTPUT_PROTOCOL_SET_MODE    SetMode;
  EFI_GRAPHICS_OUTPUT_PROTOCOL_BLT         Blt;
  EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE*       Mode;
};
#endif //UEFI_IMPL_GRAPHICS_PROTOCOLS_H
