#ifndef UEFI_IMPL_INFORMATION_H
#define UEFI_IMPL_INFORMATION_H
#include "helper-interfaces/boot_graphics_helper_interface.h"
#include "system-table/efi_system_table.h"

void output_EFI_memory_status(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* ConOut, EFI_MEMORY_DESCRIPTOR* MemoryMap,
                              UINTN NumberOfEntries, UINTN DescriptorSize) {
  ConOut->ClearScreen(ConOut);
  UINTN total_size = 0;
  for (UINTN i = 0; i < NumberOfEntries; i++) {
    auto* descriptor =  reinterpret_cast<EFI_MEMORY_DESCRIPTOR*>(reinterpret_cast<UINT64>(MemoryMap) + (i * DescriptorSize));
    ConOut->SetAttribute(ConOut, EFI_CYAN);
    ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Memory type: "));
    ConOut->OutputString(ConOut, EFI_MEMORY_TYPE_Strings[descriptor->Type]);
    ConOut->SetAttribute(ConOut, EFI_BROWN);
    ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\nPhysical Address: "));
    print_unsigned_number(ConOut, descriptor->PhysicalStart);
    ConOut->SetAttribute(ConOut, EFI_GREEN);
    ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\nVirtual Address: "));
    print_unsigned_number(ConOut, descriptor->VirtualStart);
    ConOut->SetAttribute(ConOut, EFI_YELLOW);
    ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\nNumberOfPages: "));
    print_unsigned_number(ConOut, descriptor->NumberOfPages);
    ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
    ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
    total_size = total_size + (4096 * descriptor->NumberOfPages);
  }
  ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\nTotal size: "));
  print_unsigned_number(ConOut, total_size/(1024 * 1024));
  ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"MB \r\n"));
}
#endif //UEFI_IMPL_INFORMATION_H
