#ifndef UEFI_IMPL_DEVICE_PATH_PROTOCOL_H
#define UEFI_IMPL_DEVICE_PATH_PROTOCOL_H
#include "../base/GUID.h"
#define EFI_DEVICE_PATH_PROTOCOL_GUID \
{0x09576e91,0x6d3f,0x11d2,\
{0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}

inline EFI_GUID create_device_path_protocol_GUID() {
  return EFI_GUID{0x09576e91, 0x6d3f, 0x11d2, {0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}};
}

//UEFI 2.9 spec, page 292
struct EFI_DEVICE_PATH_PROTOCOL { //NOLINT
  UINT8     Type;
  UINT8     SubType;
  UINT8     Length[2];
};
#endif //UEFI_IMPL_DEVICE_PATH_PROTOCOL_H
