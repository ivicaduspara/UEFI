#ifndef UEFI_IMPL_EFI_SYSTEM_TABLE_H
#define UEFI_IMPL_EFI_SYSTEM_TABLE_H
#include "../console-protocols/simple_text_protocols.h"
#include "../boot-services/efi_boot_services.h"
#include "../runtime-services/efi_runtime_services.h"
#include "efi_configuration_table.h"

//UEFI 2.9 specs, page 93
/*Contains definition of EFI_SYSTEM_TABLE and its header.*/
struct EFI_SYSTEM_TABLE { //NOLINT
  EFI_TABLE_HEADER                      header;
  CHAR16*                               FirmwareVendor;
  UINT32                                FirmwareVersion;
  EFI_HANDLE                            ConsoleInHandle;
  EFI_SIMPLE_TEXT_INPUT_PROTOCOL*       ConIn;
  EFI_HANDLE                            ConsoleOutHandle;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL*      ConOut;
  EFI_HANDLE                            ConsoleErrHandle;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL*      ConErr;
  EFI_RUNTIME_SERVICES*                 RuntimeServices;
  EFI_BOOT_SERVICES*                    BootServices;
  UINTN                                 NumberOfTableEntries;
  EFI_CONFIGURATION_TABLE*              ConfigurationTable;
};

#endif //UEFI_IMPL_EFI_SYSTEM_TABLE_H
