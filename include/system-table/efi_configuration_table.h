#ifndef UEFI_IMPL_EFI_CONFIGURATION_TABLE_H
#define UEFI_IMPL_EFI_CONFIGURATION_TABLE_H
#include "../base/GUID.h"

//UEFI 2.9 specs, page 101
//Definition of EFI_CONFIGURATION_TABLE
struct EFI_CONFIGURATION_TABLE {
  EFI_GUID      VendorGuid;
  void*         VendorTable;
};
#endif //UEFI_IMPL_EFI_CONFIGURATION_TABLE_H
