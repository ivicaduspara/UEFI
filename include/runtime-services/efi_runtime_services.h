#ifndef UEFI_IMPL_EFI_RUNTIME_SERVICES_H
#define UEFI_IMPL_EFI_RUNTIME_SERVICES_H
#include "../base/efi_table_header.h"
#include "../base/efi_time_services.h"
#include "memory_services.h"
#include "misc_services.h"
#include "variable_services.h"

struct EFI_RUNTIME_SERVICES;

using EFI_GET_TIME =  EFI_STATUS (*)(EFI_TIME* Time, EFI_TIME_CAPABILITIES *Capabilities);
using EFI_SET_TIME = EFI_STATUS (*)(EFI_TIME* Time);
using EFI_GET_WAKEUP_TIME = EFI_STATUS (*)(BOOLEAN* Enabled, BOOLEAN* Pending, EFI_TIME* Time);
using EFI_SET_WAKEUP_TIME = EFI_STATUS (*)(BOOLEAN Enable, EFI_TIME* Time);
using EFI_SET_VIRTUAL_ADDRESS_MAP = EFI_STATUS (*)(UINTN MemoryMapSize, UINTN DescriptorSize,
                                                   UINT32 DescriptorVersion, EFI_MEMORY_DESCRIPTOR* VirtualMap);
using EFI_CONVERT_POINTER = EFI_STATUS (*)(UINTN DebugDisposition, void** Address);
using EFI_GET_VARIABLE = EFI_STATUS (*)(CHAR16* VariableName, EFI_GUID* VendorGuid,
                                        UINT32* Attributes, UINTN* DataSize, void* Data);
using EFI_GET_NEXT_VARIABLE_NAME = EFI_STATUS (*)(UINTN* VariableNameSize, CHAR16* VariableName, EFI_GUID* VendorGuid);
using EFI_SET_VARIABLE = EFI_STATUS (*)(CHAR16* VariableName, EFI_GUID* VendorGuid, UINT32 Attributes, UINTN DataSize, void* Data);
using EFI_GET_NEXT_HIGH_MONO_COUNT = EFI_STATUS (*)(UINT32* HighCount);
using EFI_RESET_SYSTEM = EFI_STATUS (*)(EFI_RESET_TYPE ResetType, EFI_STATUS ResetStatus, UINTN DataSize, void* ResetData);
using EFI_UPDATE_CAPSULE = EFI_STATUS (*)(EFI_CAPSULE_HEADER** CapsuleHeaderArray, UINTN CapsuleCount, EFI_PHYSICAL_ADDRESS ScatterGatherList);
using EFI_QUERY_CAPSULE_CAPABILITIES = EFI_STATUS (*)(EFI_CAPSULE_HEADER** CapsuleHeaderArray,
                                                      UINTN CapsuleCount, UINT64* MaximumCapsuleSize, EFI_RESET_TYPE* ResetType);
using EFI_QUERY_VARIABLE_INFO = EFI_STATUS (*)(UINT32 Attributes, UINT64* MaximumVariableStorageSize,
                                               UINT64* RemainingVariableStorageSize, UINT64* MaximumVariableSize);
struct EFI_RUNTIME_SERVICES {
  EFI_TABLE_HEADER                    Hdr;
  EFI_GET_TIME                        GetTime;
  EFI_SET_TIME                        SetTime;
  EFI_GET_WAKEUP_TIME                 GetWakeupTime;
  EFI_SET_WAKEUP_TIME                 SetWakeupTime;
  EFI_SET_VIRTUAL_ADDRESS_MAP         SetVirtualAddressMap;
  EFI_CONVERT_POINTER                 ConvertPointer;
  EFI_GET_VARIABLE                    GetVariable;
  EFI_GET_NEXT_VARIABLE_NAME          GetNextVariableName;
  EFI_SET_VARIABLE                    SetVariable;
  EFI_GET_NEXT_HIGH_MONO_COUNT        GetNextHighMonotonicCount;
  EFI_RESET_SYSTEM                    ResetSystem;
  EFI_UPDATE_CAPSULE                  UpdateCapsule;
  EFI_QUERY_CAPSULE_CAPABILITIES      QueryCapsuleCapabilities;
  EFI_QUERY_VARIABLE_INFO             QueryVariableInfo;
};
#endif //UEFI_IMPL_EFI_RUNTIME_SERVICES_H
