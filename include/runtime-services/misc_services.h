#ifndef UEFI_IMPL_MISC_SERVICES_H
#define UEFI_IMPL_MISC_SERVICES_H
#include "../base/efi_memory_descriptor.h"
#include "../base/GUID.h"

#define CAPSULE_FLAGS_PERSIST_ACROSS_RESET 0x00010000
#define CAPSULE_FLAGS_POPULATE_SYSTEM_TABLE 0x00020000
#define CAPSULE_FLAGS_INITIATE_RESET 0x00040000

//UEFI 2.9 specs, page 267

enum  EFI_RESET_TYPE{
  EfiResetCold,
  EfiResetWarm,
  EfiResetShutdown,
  EfiResetPlatformSpecific
};

struct EFI_CAPSULE_HEADER {
  EFI_GUID      CapsuleGuid;
  UINT32        HeaderSize;
  UINT32        Flags;
  UINT32        CapsuleImageSize;
};

#endif //UEFI_IMPL_MISC_SERVICES_H
