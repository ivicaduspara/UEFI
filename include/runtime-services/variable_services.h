#ifndef UEFI_IMPL_VARIABLE_SERVICES_H
#define UEFI_IMPL_VARIABLE_SERVICES_H
#include "../base/GUID.h"

//*******************************************************
// Variable Attributes
//*******************************************************
#define EFI_VARIABLE_NON_VOLATILE 0x00000001
#define EFI_VARIABLE_BOOTSERVICE_ACCESS 0x00000002
#define EFI_VARIABLE_RUNTIME_ACCESS 0x00000004
#define EFI_VARIABLE_HARDWARE_ERROR_RECORD 0x00000008
#define EFI_VARIABLE_AUTHENTICATED_WRITE_ACCESS 0x00000010
#define EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS 0x00000020
#define EFI_VARIABLE_APPEND_WRITE 0x00000040
#define EFI_VARIABLE_ENHANCED_AUTHENTICATED_ACCESS 0x00000080
#define EFI_VARIABLE_AUTHENTICATION_3_CERT_ID_SHA256 1

using GetVariable = EFI_STATUS (*)(CHAR16* VariableName, EFI_GUID* VendorGuid, UINT32* Attributes, UINTN* DataSize, void* Data);
using GetNextVariableName  = EFI_STATUS (*)( UINTN* VariableNameSize, CHAR16* VariableName, EFI_GUID* VendorGuid);
using SetVariable = EFI_STATUS (*)(CHAR16* VariableName, EFI_GUID* VendorGuid, UINT32 Attributes, UINTN DataSize, void* Data);
using QueryVariableInfo = EFI_STATUS (*)(UINT32 Attributes, UINT64* MaximumVariableStorageSize,
                                        UINT64* RemainingVariableStorageSize, UINT64* MaximumVariableSize);
#endif //UEFI_IMPL_VARIABLE_SERVICES_H
