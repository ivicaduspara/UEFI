#ifndef UEFI_IMPL_MEMORY_SERVICES_H
#define UEFI_IMPL_MEMORY_SERVICES_H
#include "../base/efi_memory_descriptor.h"

//UEFI 2.9 specs, page 267

//*******************************************************
// EFI_OPTIONAL_PTR
//*******************************************************
#define EFI_OPTIONAL_PTR 0x00000001

using ConvertPointer = EFI_STATUS(*)(UINTN DebugDisposition, void** Address);
using SetVirtualAddressMap = EFI_STATUS(*)(UINTN MemoryMapSize, UINTN DescriptorSize,
                                           UINT32 DescriptorVersion, EFI_MEMORY_DESCRIPTOR* VirtualMap);
#endif //UEFI_IMPL_MEMORY_SERVICES_H
