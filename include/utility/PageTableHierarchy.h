#ifndef UEFI_IMPL_PAGETABLEHIERARCHY_H
#define UEFI_IMPL_PAGETABLEHIERARCHY_H
#include "../base/efi_status_codes.h"
#include "../system-table/efi_system_table.h"
#include "../utility/PageFlags.h"
#include "../utility/PT_LOAD_Descriptor.h"
#define X86_64_PHYSICAL_ADDRESS_WIDTH 48
#define X86_64_VIRTUAL_ADDRESS_WIDTH 64
#define X86_64_PAGE_SIZE 4096
#define PML4_INDEX_SHIFT 39
#define PML3_INDEX_SHIFT 30
#define PML2_INDEX_SHIFT 21
#define PML1_INDEX_SHIFT 12
#define PML_INDEX_MASK 0x1FFull
#define PML_ENTRY_MASK 0x0007FFFFFFFFF000
#define KERNEL_PHYSICAL_ADDRESS_START 0x100000
#define HIGHER_HALF_CANNONICAL_ADDRESSES_START         0xFF80000000000000ULL
#define RAM_MAP_START                                  0xFFFF888000000000ULL
#define RAM_MAP_END                                    0xFFFFFD8FFFFFFFFFULL
#define PHYSICAL_PAGE_STRUCT_MAP_START                 0xFFFFFE0000000000ULL
#define PHYSICAL_PAGE_STRUCT_MAP_END                   0xFFFFFE7FFFFFFFFFULL
#define KERNEL_VIRTUAL_ADDRESS_OFFSET                  0xFFFFFF8040000000ULL


/*
 *  ================================ UEFI and paging ========================================
 *
 * Per https://wiki.osdev.org/UEFI and to https://uefi.org/sites/default/files/resources/UEFI_Spec_2_9_2021_03_18.pdf page 27,
 * paging is enabled and space described with EfiMemoryMap is identity mapped. The overall size of EfiMemoryMap
 * depends on how you run this bootloader.
 *
 * - If you run it via emulator, it depends on how much memory you give for image being executed.
 * - If you run on physical hardware, please tell me how much memory you get because I never tried this on real hardware.
 *
 * Let's assume you ran this via emulator and gave it 512 MB of memory. This means that EfiMemoryMap should be
 * around 512MB (not necessarily that exact number). This also means that first (and only) 512 MB of memory
 * is identity mapped. Virtual address 0 corresponds to physical address 0. Virtual address of 1MB is physical
 * address 1MB, etc.
 *
 * ================================= Kernel's load address ===================================
 * Where is kernel loaded?
 *
 * It is loaded at `kernel_physical_address`. That value should match load address
 * in kernel's linker script. (Ideally, this should be moved into UEFI configuration, so it doesn't require recompilation.)
 *
 * If `kernel_physical_address` has a value larger than provided memory, it will crash. I.e., running an emulator
 * with 511 MB of physical memory (RAM) and placing kernel's load address (physical address) at 8GB will cause a crash.
 *
 * In current implementation, load address should be 1MB (0x100000). If it isn't I have to update this comment.
 *
 * ========================== HIGHER HALF KERNELS =============================================
 * On x86-64 architecture, as of the time of writing (January 2023), physical addresses are 48 bit wides.
 * Full 64-bit address space is still not enabled. Upper 16 bits [63-48] must match 47th bit.
 *
 * For variety of reasons (some of which are listed here https://wiki.osdev.org/Higher_Half_Kernel,
 * https://github.com/thomasloven/mittos64/blob/master/doc/4_Higher_Half_Kernel.md) higher half kernel is used.
 * This means that upper 16 bits are all 1. If one were to directly translate FF FF 80 00 00 00 00 00 00 (the lowest
 * legal address in higher half) into a physical address you'd get 2^47 which is 128 TB.
 *
 * This is a problem because most of the available devices/emulators (as of time of writing) do not have / can not
 * allocate such memory. This means identity mapping is not viable. This is not a problem since one can specify
 * Different virtual memory address and different load memory address in a linker script.
 *
 * */



/* Class PageTableHierarchy is responsible for creating appropriate page tables,
 * pages which contain kernel code and instances(objects) of `PhysicalPage` class.
 *
 * As was written above, during the bootloader, UEFI automatically identity maps
 * all the memory. This doesn't work for higher half kernels. To implement a higher
 * half kernel, here is what this class does:
 *
 * CONSTRUCTOR:
 * 1. Create a hierarchy of page tables for kernel code -> that is for the loaded binary file (kernel.ELF file).
 *   1a) Allocate a single page for PML 4 table
 *   1b) Allocate a single page for PML 3 table
 *   1c) Allocate a single page for PML 2 table
 *   1d) Allocate a single page for PML 1 table
 *   1e) Associate these pages in the following way:
 *       PML4[511] --> PML3 (512th entry of the PML4 table points to the PML3 table)
 *       PML3[1] --> PML2 (2nd entry of the PML3 table points to the PML2 table)
 *       PML2[0] --> PML1 (1st entry of the PML2 table points to the PML1 table)
 *   1f) Some identity mapping must be preserved. In this implementation, first 4 GB of
 *       identity mapping are preserved.
 *   1g) Allocate a single page for PML 3 table
 *   1h) Create 4 entries that represent "HUGE pages (1GB in size)) in PML 3 table allocated in 1g
 *   1i) Associate PML 3 table from 1g with PML4[0]
 *
 *
 * Map kernel pages:
 *
 * 2. Maps every page of every PT_LOAD section. If a PT_LOAD section has more than
 * one page, those pages are contiguous. With that in mind, PT_LOAD_Descriptor
 * class contains starting physical address and starting virtual address of each
 * PT_LOAD section.
 *
 * All of these should map from 0xFFFFFF8040000000ULL to 0xFFFFFFFFFFFFFFFFULL
 * (PML4[511] and all tables in that tree are reserved for kernel).
 *
 *
 * Map entire RAM:
 *
 * 3. Map entire RAM. This will prevent any headaches when trying
 * to write to some page in kernel and getting a page fault or a triple fault.
 *
 * Mapping for these starts at PML4[273] or 0xFFFF888000000000ULL
 * and ends at 0xFFFFC87FFFFFFFFFULL
 *
 * Map physical page struct array:
 *
 * 4. This array contains an entry for every physical page. This is needed
 * for buddy allocator, and being able to get/free physical pages.
 *
 * */


class PageTableHierarchy {
public:
  /**
 * When creating a PageTableHierarchy, At least one page is needed for the
 * PML4 table, one page for a PDPT(PML3) table, one page for a PDT(PML2) table
 * and a one for PT(PML1) table. This makes 4 pages.
 *
 * Inside PT(PML1) table in use, pages which are present should correspond
 * to PT_LOAD sections, which is to say that pages containing the contents of PT_LOAD
 * sections should be assigned to the appropriate PML1 table.
 *
 * Additionally, because of UEFI bugs lower physical memory will have to be mapped.
 * For now, 4GB of lower memory is mapped, kernel is mapped on a separate location
 * */
 explicit PageTableHierarchy(EFI_SYSTEM_TABLE* SystemTable);

  void MapKernelPages(EFI_SYSTEM_TABLE* SystemTable,
                      PT_LOAD_Descriptor* descriptors);

  void MapEntireRAM(EFI_SYSTEM_TABLE* SystemTable) noexcept;

  [[nodiscard]] UINT64* get_PML4_ptr_raw() const {
    return PML4_pointer;
  }

  [[nodiscard]] UINT64* get_page_struct_array_start() const {
    return page_struct_array_start;
  }

  [[nodiscard]] UINT64 get_psa_size() const {
    return psa_size;
  }

  void DebugPrintPageHierarchy(EFI_SYSTEM_TABLE* SystemTable) noexcept;

  void DebugPrintPML4SubtreeAtIndex(EFI_SYSTEM_TABLE* SystemTable, UINTN index) noexcept;

private:
  constexpr static UINT64 kernel_max_alloc_address = (1ull << X86_64_PHYSICAL_ADDRESS_WIDTH) - 1;

  EFI_STATUS MapPages(EFI_SYSTEM_TABLE* SystemTable,
                      UINT64 Count, UINT64 physical_address, UINT64 virtual_address, UINT64 flags,
                      EFI_ALLOCATE_TYPE allocation_type);

  EFI_STATUS MapSinglePage(EFI_SYSTEM_TABLE* SystemTable,
                           UINT64 physical_address, UINT64 virtual_address, UINT64 flags,
                           EFI_ALLOCATE_TYPE allocation_type);

  void ZeroOut(UINT8* start_address, UINTN count);

  /* PML4_pointer points to an address where PML4 (PML4[0]) is found.
   *
   * */
  UINT64* PML4_pointer{nullptr};

  UINT64* page_struct_array_start{nullptr};

  UINT64 psa_size;
};
#endif // UEFI_IMPL_PAGETABLEHIERARCHY_H
