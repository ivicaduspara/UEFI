#ifndef UEFI_IMPL_PAGEFLAGS_H
#define UEFI_IMPL_PAGEFLAGS_H
#include "../base/efi_types.h"
enum PageFlags : uint64_t{
  Present = (1ull << 0),
  Write = (1ull << 1),
  User = (1ull << 2),
  PWT = (1ull << 3),
  PCD = (1ull << 4),
  Accessed = (1ull << 5),
  Dirty = (1ull << 6),
  Size = (1ull << 7),
  Global = (1ull << 8),
  NX = (1ull << 63),
  Kernel_RO = Present | NX,
  Kernel_RW = Present | NX | Write
};
#endif // UEFI_IMPL_PAGEFLAGS_H
