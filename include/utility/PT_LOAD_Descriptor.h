#ifndef UEFI_IMPL_PT_LOAD_DESCRIPTOR_H
#define UEFI_IMPL_PT_LOAD_DESCRIPTOR_H
#include "../base/efi_types.h"

/* For every PT_LOAD segment in ELF file, we have to allocate pages.
 * Important fields to save from every PT_LOAD segments of ELF file are:
 *
 * Flags of that PT_LOAD segment (flags describe how page should behave)
 * Physical address at which PT_LOAD segment is placed.
 * Virtual address of that PT_LOAD segment.
 * Number of pages allocated by that PT_LOAD segment
 * Pointer to the next allocated PageDescriptor. If it is nullptr, end of linked list.
 *
 * */
struct PT_LOAD_Descriptor { //NOLINT
  UINT64 physical_address;
  UINT64 virtual_address;
  UINTN number_pages;
  UINT32 flags;
  PT_LOAD_Descriptor* next_descriptor{nullptr};
};
#endif // UEFI_IMPL_PT_LOAD_DESCRIPTOR_H
