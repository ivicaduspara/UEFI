#ifndef UEFI_IMPL_UTILITY_H
#define UEFI_IMPL_UTILITY_H
#include "../system-table/efi_system_table.h"

/**
 * Contains some utility functions.
 * */
INT32 memcmp(const void* address1, const void* address2, SIZE_T n, EFI_SYSTEM_TABLE* SystemTable) {
  const auto* a1 = reinterpret_cast<const UINT8*>(address1);
  const auto* a2 = reinterpret_cast<const UINT8*>(address2);
  for(SIZE_T i = 0; i < n; i++) {
    if (a1[i] < a2[i]) {
      return -1;
    }
    else if (a1[i] > a2[i]) {
      return 1;
    }
  }
  return 0;
}

void print_current_date(EFI_SYSTEM_TABLE* SystemTable) {
  EFI_TIME current_time;
  SystemTable->RuntimeServices->GetTime(&current_time, nullptr);
  SystemTable->ConOut->SetAttribute(SystemTable->ConOut, EFI_RED);
  print_unsigned_number(SystemTable->ConOut, current_time.Day);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"."));
  print_unsigned_number(SystemTable->ConOut, current_time.Month);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"."));
  print_unsigned_number(SystemTable->ConOut, current_time.Year);
  SystemTable->ConOut->OutputString(SystemTable->ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
}

#endif //UEFI_IMPL_UTILITY_H
