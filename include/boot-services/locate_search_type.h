#ifndef UEFI_IMPL_LOCATE_SEARCH_TYPE_H
#define UEFI_IMPL_LOCATE_SEARCH_TYPE_H
//UEFI 2.9 spec, page 187
enum EFI_LOCATE_SEARCH_TYPE{
  AllHandles,
  ByRegisterNotify,
  ByProtocol
};
#endif // UEFI_IMPL_LOCATE_SEARCH_TYPE_H