#ifndef UEFI_IMPL_PROTOCOL_INFORMATION_ENTRY_H
#define UEFI_IMPL_PROTOCOL_INFORMATION_ENTRY_H
#include "../base/efi_types.h"

//UEFI 2.9 spec, page 202
struct  EFI_OPEN_PROTOCOL_INFORMATION_ENTRY { //NOLINT
  EFI_HANDLE      AgentHandle;
  EFI_HANDLE      ControllerHandle;
  UINT32          Attributes;
  UINT32          OpenCount;
};
#endif //UEFI_IMPL_PROTOCOL_INFORMATION_ENTRY_H
