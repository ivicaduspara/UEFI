#ifndef UEFI_IMPL_EFI_BOOT_SERVICES_H
#define UEFI_IMPL_EFI_BOOT_SERVICES_H
#include "../base/efi_memory_descriptor.h"
#include "../base/efi_table_header.h"
#include "../device-path-protocols/device_path_protocol.h"

#include "efi_event.h"
#include "efi_task_priority_levels.h"
#include "locate_search_type.h"
#include "protocol_handler_services.h"
#include "protocol_information_entry.h"
#include "timer.h"

struct EFI_BOOT_SERVICES;

//Aliases, in order of appearance inside of EFI_BOOT_SERVICES, spec defines them alphabetically

// Task priority services
using EFI_RAISE_TPL = EFI_TPL(*)(EFI_TPL NewTpl);
using EFI_RESTORE_TPL = void(*)(EFI_TPL OldTpl);

// Memory services
using EFI_ALLOCATE_PAGES = EFI_STATUS(*)(EFI_ALLOCATE_TYPE Type, EFI_MEMORY_TYPE MemoryType, UINTN Pages, EFI_PHYSICAL_ADDRESS* Memory);
using EFI_FREE_PAGES = EFI_STATUS(*)(EFI_PHYSICAL_ADDRESS Memory, UINTN Pages);
using EFI_GET_MEMORY_MAP = EFI_STATUS(*)(UINTN* MemoryMapSize, EFI_MEMORY_DESCRIPTOR* MemoryMap, UINTN* MapKey,
                                         UINTN* DescriptorSize, UINT32* DescriptorVersion);
using EFI_ALLOCATE_POOL = EFI_STATUS(*)(EFI_MEMORY_TYPE PoolType, UINTN Size, void** Buffer);
using EFI_FREE_POOL = EFI_STATUS(*)(void* Buffer);

// Event & Timer Services
using EFI_CREATE_EVENT = EFI_STATUS(*)(UINT32 Type, EFI_TPL NotifyTpl, EFI_EVENT_NOTIFY NotifyFunction,
                                       void* NotifyContext, EFI_GUID* EventGroup, EFI_EVENT* Event);
using EFI_SET_TIMER =  EFI_STATUS (*)(EFI_EVENT Event, EFI_TIMER_DELAY Type, UINT64 TriggerTime);
using EFI_WAIT_FOR_EVENT = EFI_STATUS (*)(UINTN NumberOfEvents, EFI_EVENT* Event, UINTN* Index);
using EFI_SIGNAL_EVENT = EFI_STATUS (*)(EFI_EVENT Event);
using EFI_CLOSE_EVENT = EFI_STATUS (*)(EFI_EVENT Event);
using EFI_CHECK_EVENT =  EFI_STATUS (*)(EFI_EVENT Event);

// Protocol Handler Services
using EFI_INSTALL_PROTOCOL_INTERFACE =  EFI_STATUS (*)(EFI_HANDLE* Handle, EFI_GUID* Protocol, EFI_INTERFACE_TYPE InterfaceType, void* Interface);
using EFI_REINSTALL_PROTOCOL_INTERFACE = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID* Protocol, void *OldInterface, void* NewInterface);
using EFI_UNINSTALL_PROTOCOL_INTERFACE = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID* Protocol, void *Interface);
using EFI_HANDLE_PROTOCOL = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID *Protocol, void** Interface);
using EFI_REGISTER_PROTOCOL_NOTIFY = EFI_STATUS (*)(EFI_GUID* Protocol, EFI_EVENT Event, void** Registration);
using EFI_LOCATE_HANDLE =  EFI_STATUS (*)(EFI_LOCATE_SEARCH_TYPE SearchType, EFI_GUID* Protocol, void *SearchKey, UINTN* BufferSize, EFI_HANDLE* Buffer);
using EFI_LOCATE_DEVICE_PATH =  EFI_STATUS (*)(EFI_GUID* Protocol, EFI_DEVICE_PATH_PROTOCOL** DevicePath, EFI_HANDLE* Device);
using EFI_INSTALL_CONFIGURATION_TABLE = EFI_STATUS (*)(EFI_GUID* Guid, void* Table);

// Image Services
using EFI_IMAGE_LOAD = EFI_STATUS (*)(BOOLEAN BootPolicy, EFI_HANDLE ParentImageHandle,
                                      EFI_DEVICE_PATH_PROTOCOL* DevicePath, void* SourceBuffer,
                                      UINTN SourceSize, EFI_HANDLE* ImageHandle);
using EFI_IMAGE_START = EFI_STATUS (*)(EFI_HANDLE ImageHandle, UINTN* ExitDataSize, CHAR16** ExitData);
using EFI_EXIT =  EFI_STATUS (*)(EFI_HANDLE ImageHandle, EFI_STATUS ExitStatus, UINTN ExitDataSize, CHAR16* ExitData);
using EFI_IMAGE_UNLOAD = EFI_STATUS (*)(EFI_HANDLE ImageHandle);
using EFI_EXIT_BOOT_SERVICES = EFI_STATUS (*)(EFI_HANDLE ImageHandle, UINTN MapKey);

// Miscellaneous Services
using EFI_GET_NEXT_MONOTONIC_COUNT =  EFI_STATUS (*)(UINT64* Count);
using EFI_STALL = EFI_STATUS (*)(UINTN Microseconds);
using EFI_SET_WATCHDOG_TIMER = EFI_STATUS (*)(UINTN Timeout, UINT64 WatchdogCode, UINTN DataSize, CHAR16* WatchdogData);

// DriverSupport Services
using EFI_CONNECT_CONTROLLER = EFI_STATUS (*)(EFI_HANDLE ControllerHandle, EFI_HANDLE* DriverImageHandle,
                                              EFI_DEVICE_PATH_PROTOCOL* RemainingDevicePath, BOOLEAN Recursive);
using EFI_DISCONNECT_CONTROLLER = EFI_STATUS (*)(EFI_HANDLE ControllerHandle, EFI_HANDLE DriverImageHandle, EFI_HANDLE ChildHandle);

// Open and Close Protocol Services
using EFI_OPEN_PROTOCOL = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID* Protocol, void** Interface,
                                         EFI_HANDLE AgentHandle, EFI_HANDLE ControllerHandle, UINT32 Attributes);
using EFI_CLOSE_PROTOCOL = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID* Protocol, EFI_HANDLE AgentHandle, EFI_HANDLE ControllerHandle);
using EFI_OPEN_PROTOCOL_INFORMATION = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID* Protocol,
                                                     EFI_OPEN_PROTOCOL_INFORMATION_ENTRY** EntryBuffer, UINTN* EntryCount);

// Library Services
using EFI_PROTOCOLS_PER_HANDLE = EFI_STATUS (*)(EFI_HANDLE Handle, EFI_GUID*** ProtocolBuffer, UINTN* ProtocolBufferCount);
using EFI_LOCATE_HANDLE_BUFFER = EFI_STATUS (*)(EFI_LOCATE_SEARCH_TYPE SearchType, EFI_GUID* Protocol, void* SearchKey,
                                                UINTN* NoHandles, EFI_HANDLE** Buffer);
using EFI_LOCATE_PROTOCOL = EFI_STATUS (*)(EFI_GUID* Protocol, void* Registration, void** Interface);
using EFI_INSTALL_MULTIPLE_PROTOCOL_INTERFACES = EFI_STATUS (*)(EFI_HANDLE* Handle, ...);
using EFI_UNINSTALL_MULTIPLE_PROTOCOL_INTERFACES = EFI_STATUS (*)(EFI_HANDLE* Handle, ...);

// 32-bit CRC Services
using EFI_CALCULATE_CRC32 = EFI_STATUS (*)(void* Data, UINTN DataSize, UINT32* Crc32);

// Miscellaneous Services
using EFI_COPY_MEM = EFI_STATUS (*)(void* Destination, void* Source, UINTN Length);
using EFI_SET_MEM = EFI_STATUS (*)(void* Buffer, UINTN Size, UINT8 Value);
using EFI_CREATE_EVENT_EX = EFI_STATUS (*)(UINT32 Type, EFI_TPL NotifyTpl, EFI_EVENT_NOTIFY NotifyFunction,
                                           void* NotifyContext, EFI_GUID* EventGroup, EFI_EVENT* Event);

struct EFI_BOOT_SERVICES { //NOLINT
  EFI_TABLE_HEADER                                Hdr;

  // Task Priority Services
  EFI_RAISE_TPL                                   RaiseTPL;
  EFI_RESTORE_TPL                                 RestoreTPL;

  // Memory Services
  EFI_ALLOCATE_PAGES                              AllocatePages;
  EFI_FREE_PAGES                                  FreePages;
  EFI_GET_MEMORY_MAP                              GetMemoryMap;
  EFI_ALLOCATE_POOL                               AllocatePool;
  EFI_FREE_POOL                                   FreePool;

  // Event & Timer Services
  EFI_CREATE_EVENT                                CreateEvent;
  EFI_SET_TIMER                                   SetTimer;
  EFI_WAIT_FOR_EVENT                              WaitForEvent;
  EFI_SIGNAL_EVENT                                SignalEvent;
  EFI_CLOSE_EVENT                                 CloseEvent;
  EFI_CHECK_EVENT                                 CheckEvent;

  // Protocol Handler Services
  EFI_INSTALL_PROTOCOL_INTERFACE                  InstallProtocolInterface;
  EFI_REINSTALL_PROTOCOL_INTERFACE                ReinstallProtocolInterface;
  EFI_UNINSTALL_PROTOCOL_INTERFACE                UninstallProtocolInterface;
  EFI_HANDLE_PROTOCOL                             HandleProtocol;
  void*                                           Reserved;
  EFI_REGISTER_PROTOCOL_NOTIFY                    RegisterProtocolNotify;
  EFI_LOCATE_HANDLE                               LocateHandle;
  EFI_LOCATE_DEVICE_PATH                          LocateDevicePath;
  EFI_INSTALL_CONFIGURATION_TABLE                 InstallConfigurationTable;

  // Image Services
  EFI_IMAGE_LOAD                                  LoadImage;
  EFI_IMAGE_START                                 StartImage;
  EFI_EXIT                                        Exit;
  EFI_IMAGE_UNLOAD                                UnloadImage;
  EFI_EXIT_BOOT_SERVICES                          ExitBootServices;

  // Miscellaneous Services
  EFI_GET_NEXT_MONOTONIC_COUNT                    GetNextMonotonicCount;
  EFI_STALL                                       Stall;
  EFI_SET_WATCHDOG_TIMER                          SetWatchdogTimer;

  // DriverSupport Services
  EFI_CONNECT_CONTROLLER                          ConnectController;
  EFI_DISCONNECT_CONTROLLER                       DisconnectController;

  // Open and Close Protocol Services
  EFI_OPEN_PROTOCOL                               OpenProtocol;
  EFI_CLOSE_PROTOCOL                              CloseProtocol;
  EFI_OPEN_PROTOCOL_INFORMATION                   OpenProtocolInformation;

  // Library Services
  EFI_PROTOCOLS_PER_HANDLE                        ProtocolsPerHandle;
  EFI_LOCATE_HANDLE_BUFFER                        LocateHandleBuffer;
  EFI_LOCATE_PROTOCOL                             LocateProtocol;
  EFI_INSTALL_MULTIPLE_PROTOCOL_INTERFACES        InstallMultipleProtocolInterfaces;
  EFI_UNINSTALL_MULTIPLE_PROTOCOL_INTERFACES      UninstallMultipleProtocolInterfaces;

  // 32-bit CRC Services
  EFI_CALCULATE_CRC32                             CalculateCrc32;

  // Miscellaneous Services
  EFI_COPY_MEM                                    CopyMem;
  EFI_SET_MEM                                     SetMem;
  EFI_CREATE_EVENT_EX                             CreateEventEx;
};
#endif //UEFI_IMPL_EFI_BOOT_SERVICES_H
