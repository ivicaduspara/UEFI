#ifndef UEFI_IMPL_TIMER_H
#define UEFI_IMPL_TIMER_H

//UEFI 2.9 spec, page 160
enum EFI_TIMER_DELAY {
  TimerCancel,
  TimerPeriodic,
  TimerRelative
};
#endif //UEFI_IMPL_TIMER_H
