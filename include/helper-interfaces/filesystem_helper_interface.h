#ifndef UEFI_IMPL_FILESYSTEM_HELPER_INTERFACE_H
#define UEFI_IMPL_FILESYSTEM_HELPER_INTERFACE_H
#include "../base/efi_status_codes.h"
#include "../file-protocols/file_system_protocol.h"
#include "../loaded-image-protocols/loaded_image_protocol.h"
class Filesystem_Helper{
 private:
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL* volume_;
  EFI_LOADED_IMAGE_PROTOCOL* loaded_image_;
  EFI_DEVICE_PATH_PROTOCOL* device_path_;
  EFI_FILE_PROTOCOL* root_fs_;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* con_out_;
  EFI_SYSTEM_TABLE* system_table_; //NON OWNING
  EFI_GUID efi_file_info_guid_ = create_efi_file_info_GUID();
 public:
  explicit Filesystem_Helper(EFI_SYSTEM_TABLE* systemTable, EFI_HANDLE imageHandle){ //NOLINT
    system_table_ = systemTable;
    con_out_ = systemTable->ConOut;

    //Loading image
    con_out_->SetAttribute(con_out_, EFI_BROWN);
    con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Loading image... "));
    auto efi_loaded_image_protocol_GUID = create_efi_loaded_image_protocol_GUID();
    auto efi_device_path_protocol_GUID = create_device_path_protocol_GUID();
    auto efi_simple_filesystem_protocol_GUID = create_efi_simple_file_system_protocol_GUID();
    auto image_status = systemTable->BootServices->HandleProtocol(imageHandle,
                                                                  &efi_loaded_image_protocol_GUID,
                                                                  reinterpret_cast<void**>(&loaded_image_));
    con_out_->SetAttribute(con_out_, EFI_CYAN);
    con_out_->OutputString(con_out_, CheckStandardEFIError(image_status));
    con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));

    //Device path
    con_out_->SetAttribute(con_out_, EFI_BROWN);
    con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Loading device path... "));
    auto device_path_status = systemTable->BootServices->HandleProtocol(loaded_image_->DeviceHandle,
                                                                        &efi_device_path_protocol_GUID,
                                                                        reinterpret_cast<void**>(&device_path_));
    con_out_->SetAttribute(con_out_, EFI_CYAN);
    con_out_->OutputString(con_out_, CheckStandardEFIError(device_path_status));
    con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));

    //Volume load
    con_out_->SetAttribute(con_out_, EFI_BROWN);
    con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Loading file system volume... "));
    auto fs_status = systemTable->BootServices->HandleProtocol(loaded_image_->DeviceHandle,
                                                               &efi_simple_filesystem_protocol_GUID,
                                                               reinterpret_cast<void**>(&volume_));

    con_out_->SetAttribute(con_out_, EFI_CYAN);
    con_out_->OutputString(con_out_, CheckStandardEFIError(fs_status));
    con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
    auto volume_status = volume_->OpenVolume(volume_, &root_fs_);
    if (volume_status == EFI_SUCCESS) {
      con_out_->SetAttribute(con_out_, EFI_BROWN);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"File system mounted "));
      con_out_->SetAttribute(con_out_, EFI_GREEN);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"successfully "));
      con_out_->SetAttribute(con_out_, EFI_BROWN);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"at root directory of EFI partition.\r\n\r\n"));
    } else {
      con_out_->SetAttribute(con_out_, EFI_BROWN);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"File system wasn't mounted successfully. Error: "));
      con_out_->SetAttribute(con_out_, EFI_RED);
      con_out_->OutputString(con_out_, CheckStandardEFIError(volume_status));
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n\r\n"));
    }
    con_out_->SetAttribute(con_out_, EFI_BROWN);
  }
  Filesystem_Helper() = delete;
  Filesystem_Helper(const Filesystem_Helper&) = delete;
  Filesystem_Helper(Filesystem_Helper&& ) = delete;
  Filesystem_Helper& operator=(const Filesystem_Helper&) = delete;
  Filesystem_Helper& operator=(Filesystem_Helper&& ) = delete;
  void* operator new(size_t size) = delete;
  void* operator new (size_t size, void* ptr) noexcept = delete;

  EFI_FILE_PROTOCOL* open_file(CHAR16* file_name, UINT64 file_permission_flags) {
    EFI_FILE_PROTOCOL* file_handle{nullptr};
    auto status = root_fs_->Open(root_fs_, &file_handle, file_name, file_permission_flags, 0);
    if (status != EFI_SUCCESS) {
      con_out_->SetAttribute(con_out_, EFI_RED);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Could not open the file. Received this error: "));
      con_out_->OutputString(con_out_, CheckStandardEFIError(status));
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\nPath to the file is: "));
      con_out_->OutputString(con_out_, file_name);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
    }
    return file_handle;
  }

  EFI_STATUS read_file(EFI_FILE_PROTOCOL* file_handle, void** read_buffer, UINTN* number_of_bytes_read) {
    EFI_FILE_INFO* file_info{nullptr};
    UINTN efi_file_info_size = 0UL;
    EFI_STATUS status;

    //1. Allocate memory for file_info struct, and read it.

    //This is necessary in order to obtain file info size. sizeof() didn't work here.
    file_handle->GetInfo(file_handle, &efi_file_info_guid_, &efi_file_info_size, nullptr);
    status = system_table_->BootServices->AllocatePool(EfiLoaderData, efi_file_info_size, reinterpret_cast<void**>(&file_info));
    if (status != EFI_SUCCESS) {
      con_out_->SetAttribute(con_out_, EFI_RED);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Could not allocate memory for file info. Received this error: "));
      con_out_->OutputString(con_out_, CheckStandardEFIError(status));
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
      con_out_->SetAttribute(con_out_, EFI_WHITE);
      return status;
    }

    status = file_handle->GetInfo(file_handle, &efi_file_info_guid_, &efi_file_info_size, file_info);
    if (status != EFI_SUCCESS) {
      con_out_->SetAttribute(con_out_, EFI_RED);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Could not obtain file info. Received this error: "));
      con_out_->OutputString(con_out_, CheckStandardEFIError(status));
      system_table_->BootServices->FreePool(reinterpret_cast<void*>(file_info));
      return status;
    }

    //2. Read entire file
    *number_of_bytes_read = file_info->FileSize;
    file_handle->SetPosition(file_handle, 0UL);

    status = file_handle->Read(file_handle, number_of_bytes_read, *read_buffer);
    if (status != EFI_SUCCESS) {
      con_out_->SetAttribute(con_out_, EFI_RED);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Could not read file. Received this error: "));
      con_out_->OutputString(con_out_, CheckStandardEFIError(status));
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
      system_table_->BootServices->FreePool(reinterpret_cast<void*>(file_info));
      return status;
    }

    system_table_->BootServices->FreePool(reinterpret_cast<void*>(file_info));
    return EFI_SUCCESS;
  }

  EFI_FILE_PROTOCOL* open_directory(CHAR16* dir_name, UINT64 file_permission_flags) {
    EFI_FILE_PROTOCOL* dir_handle{nullptr};
    auto status = root_fs_->Open(root_fs_, &dir_handle, dir_name, file_permission_flags, EFI_FILE_DIRECTORY);
    if (status != EFI_SUCCESS) {
      con_out_->SetAttribute(con_out_, EFI_RED);
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"Could not open the directory. Received this error: "));
      con_out_->OutputString(con_out_, CheckStandardEFIError(status));
      con_out_->OutputString(con_out_, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\nPath to the file is: "));
      con_out_->OutputString(con_out_, dir_name);
    }
    return dir_handle;
  }

  EFI_STATUS close(EFI_FILE_PROTOCOL* file_handle) { //NOLINT
    return file_handle->Close(file_handle);
  }
};
#endif //UEFI_IMPL_FILESYSTEM_HELPER_INTERFACE_H
