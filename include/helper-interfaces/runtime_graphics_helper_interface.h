#ifndef UEFI_IMPL_RUNTIME_GRAPHICS_HELPER_INTERFACE_H
#define UEFI_IMPL_RUNTIME_GRAPHICS_HELPER_INTERFACE_H
#include "../graphics-protocols/graphics_protocols.h"
/**
 * This structure is passed to the kernel. Kernel doesn't know the definition
 * of EFI_GRAPHICS_OUTPUT_PROTOCOL nor can it access those definitions. For this reason,
 * class should be created via Create_Runtime_Graphics free function.
 *
 * */

struct Frame_Buffer { // NOLINT
 public:
  void*            base_address_;
  unsigned long long   buffer_size_;
  unsigned int     screen_width_;
  unsigned int     screen_height_;
  unsigned int     pixels_per_scanline_;

 public:
   Frame_Buffer() = default;
  void* operator new(size_t size) = delete;
  void* operator new (size_t size, void* ptr) noexcept = delete;

  void set_base_address(void* base_address) {
    base_address_ = base_address;
  }

  void set_buffer_size(unsigned long buffer_size) {
    buffer_size_ = buffer_size;
  }

  void set_screen_width(unsigned int screen_width) {
    screen_width_ = screen_width;
  }

  void set_screen_height(unsigned int screen_height) {
    screen_height_ = screen_height;
  }

  void set_pixels_per_scanline(unsigned int pixels_per_scanline) {
    pixels_per_scanline_ = pixels_per_scanline;
  }

  void fill_buffer_box(unsigned int start_x, unsigned int start_y, unsigned int w, unsigned int h, unsigned int color) {
    //Drawing is done by assigning to an address of video frame buffer. What is assigned is a 32 bit integer which represents color.
    auto* casted_base_address = reinterpret_cast<unsigned char*>(base_address_);
    unsigned int pixel_size = 4;
    if (start_x < 0) { start_x = 0; }
    if (start_y < 0) { start_y = 0; }
    if (w < 1) { w = 1; }
    if (h < 1) { h = 1; }
    unsigned int width = (start_x + w) * pixel_size;
    unsigned int height = start_y + h;
    for(unsigned int y = start_y; y < height; y++) {
      for(unsigned int x = start_x * pixel_size; x < width; x+=pixel_size) {
        //Row offset is: number of the row (y) times pixels in scanline (number of pixels in horizontal row) and size of a pixel in bytes:
        //y * pixels_per_scanline_ * pixel_size. This chooses a row. To choose a position in a row offset it by x -> column
        //C++ doesn't allow void* arithmetic. For that reason, few jumps need to be made
        *reinterpret_cast<unsigned int*>(casted_base_address + (y * pixels_per_scanline_ * pixel_size) + x) = color;
      }
    }
  }
};

Frame_Buffer Create_Runtime_Graphics(EFI_GRAPHICS_OUTPUT_PROTOCOL* gop) {
  Frame_Buffer runtime_graphics;

  //Value written inside gop->Mode->FrameBufferBase is the **address** of the frame buffer.
  //Do not take the address of the variable which stores the frame buffer's address, instead treat it
  //as address.
  runtime_graphics.set_base_address(reinterpret_cast<void*>(gop->Mode->FrameBufferBase));
  runtime_graphics.set_buffer_size(gop->Mode->FrameBufferSize);
  runtime_graphics.set_screen_width(gop->Mode->Info->HorizontalResolution);
  runtime_graphics.set_screen_height(gop->Mode->Info->VerticalResolution);
  runtime_graphics.set_pixels_per_scanline(gop->Mode->Info->PixelsPerScanLine);
  return runtime_graphics;
}
#endif // UEFI_IMPL_RUNTIME_GRAPHICS_HELPER_INTERFACE_H
