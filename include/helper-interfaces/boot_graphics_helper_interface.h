#ifndef UEFI_IMPL_BOOT_GRAPHICS_HELPER_INTERFACE_H
#define UEFI_IMPL_BOOT_GRAPHICS_HELPER_INTERFACE_H
#include "../base/efi_status_codes.h"
#include "../font/extended_ascii_table.h"
#include "../graphics-protocols/graphics_protocols.h"
#include "../system-table/efi_system_table.h"
/* This header file does not define a struct specified in UEFI specs. It is simply, a convenience header
 * file which defines a shortuct functions for drawing graphics, setting color, clearing screen, etc.*/

/* EFI_SYSTEM_TABLE pointer is provided inside entry function. That is the pointer which provides
   * LocateProtocol function with which graphics protocol can be located.
   *
   * There is a possibility of this helper class outliving the EFI_SYSTEM_TABLE* which located the protocol. To
   * prevent it, this class is forbidden from being allocated on heap, copied, moved or created uninitialized
   *
   * I guess you can make a global variable out of this, but I can't really stop that...
   *
   * This is not usable after call to EXIT_BOOT_SERVICES();
   * */
struct EFI_Boot_Graphics_Helper{ //NOLINT
 private:
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL output_color_{0xFF,0xFF, 0x00, 0x00};
  EFI_GUID EFI_GO_GUID_ = create_efi_graphics_output_protocol_GUID();
  EFI_GRAPHICS_OUTPUT_PROTOCOL* efi_gop_;
 public:
  EFI_Boot_Graphics_Helper() = delete;
  explicit EFI_Boot_Graphics_Helper(EFI_SYSTEM_TABLE* efiSystemTable) { //NOLINT
    efiSystemTable->BootServices->LocateProtocol(&EFI_GO_GUID_, nullptr, reinterpret_cast<void**>(&efi_gop_));
  }
  EFI_Boot_Graphics_Helper(const EFI_Boot_Graphics_Helper&) = delete;
  EFI_Boot_Graphics_Helper(EFI_Boot_Graphics_Helper&& ) = delete;
  EFI_Boot_Graphics_Helper& operator=(const EFI_Boot_Graphics_Helper&) = delete;
  EFI_Boot_Graphics_Helper& operator=(EFI_Boot_Graphics_Helper&& ) = delete;
  void* operator new(size_t size) = delete;
  void* operator new (size_t size, void* ptr) noexcept = delete;

  void Set_Color(UINT32 color) {
    output_color_.Reserved = color >> 24;
    output_color_.Red = color >> 16;
    output_color_.Green = color >> 8;
    output_color_.Blue = color;
  }

  void Fill_Rectangle(UINTN SourceX, UINTN SourceY, UINTN DestinationX, UINTN DestinationY,
                      UINTN Width, UINTN Height, UINTN Delta,
                      EFI_GRAPHICS_OUTPUT_BLT_OPERATION operation = EFI_GRAPHICS_OUTPUT_BLT_OPERATION::EfiBltVideoFill) {
    efi_gop_->Blt(efi_gop_, &output_color_, operation, SourceX, SourceY, DestinationX, DestinationY, Width ,Height, Delta);

  }

  void Fill_Pixel(UINTN DestinationX, UINTN DestinationY,
                  EFI_GRAPHICS_OUTPUT_BLT_OPERATION operation = EFI_GRAPHICS_OUTPUT_BLT_OPERATION::EfiBltVideoFill) {
    efi_gop_->Blt(efi_gop_, &output_color_, EfiBltVideoFill, 0, 0, 50, 50, 100 ,200, 0);
  }

  EFI_STATUS Draw_Extended_ASCII_Character(UINT32 CharIndex, UINTN XCoord, UINTN YCoord, UINTN FontSize) {
    if (FontSize < 4) {
      FontSize = 4;
    }
    auto FS = static_cast<UINT32>(FontSize/4);
    UINTN pixel_column_position = 0; //[0-7].
    UINTN monitor_X_coordinate = XCoord;
    UINTN monitor_Y_coordinate = YCoord;
    //Char 0 is at index 0*128, char 1 is at index 1*128, etc. Ensure that CharIndex is properly aligned.
    if (CharIndex < 0 || CharIndex > 255) {
      return EFI_INVALID_PARAMETER;
    }
    for (UINTN i = 128 * CharIndex; i < 128 * (CharIndex + 1); i++) {
      //Check if 'carriage' needs to be returned to beginning and new line activated
      if (pixel_column_position > 7) {
        pixel_column_position = 0;
        monitor_Y_coordinate += FS;
        monitor_X_coordinate = XCoord;
      }
      auto should_draw_pixel = extendedn_ascii_font[i];
      if (should_draw_pixel) {
        Fill_Rectangle(0, 0, monitor_X_coordinate, monitor_Y_coordinate, FS, FS, 0);
      }
      monitor_X_coordinate += FS;
      pixel_column_position++;
    }
    return EFI_SUCCESS;
  }
};
#endif // UEFI_IMPL_BOOT_GRAPHICS_HELPER_INTERFACE_H
