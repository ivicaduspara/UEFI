#ifndef UEFI_IMPL_RUNTIME_PSF1_HELPER_H
#define UEFI_IMPL_RUNTIME_PSF1_HELPER_H
#include "filesystem_helper_interface.h"
#define PSF1_MAGIC0 0x36
#define PSF1_MAGIC1 0x04

struct PSF1_HEADER {
  unsigned char magic[2];
  unsigned char mode;
  unsigned char char_size;
};

struct PSF1_FONT {
  PSF1_HEADER* psf1_header;
  void* glyph_buffer;
};

PSF1_FONT* load_PSF1_font(EFI_HANDLE* ImageHandle, EFI_SYSTEM_TABLE* SystemTable, Filesystem_Helper* filesystem_helper) {
  auto font_file = filesystem_helper->open_file(CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"zap-light16.psf"), EFI_FILE_MODE_READ);
  if (font_file == nullptr) {
    return nullptr;
  }
  PSF1_HEADER* font_header;
  SystemTable->BootServices->AllocatePool(EfiLoaderData, sizeof(PSF1_HEADER), reinterpret_cast<void**>(&font_header));
  UINTN font_header_size = sizeof(PSF1_HEADER);
  font_file->Read(font_file,&font_header_size,font_header);

  if (font_header->magic[0] != PSF1_MAGIC0 || font_header->magic[1] != PSF1_MAGIC1) {
    return nullptr;
  }

  UINTN glyph_buffer_size = font_header->char_size * 256;
  if (font_header->mode == 1) {
    glyph_buffer_size = font_header->char_size * 512;
  }
  void* glyph_buffer;
  font_file->SetPosition(font_file, sizeof(PSF1_HEADER));
  SystemTable->BootServices->AllocatePool(EfiLoaderData, glyph_buffer_size, reinterpret_cast<void**>(&glyph_buffer));
  font_file->Read(font_file, &glyph_buffer_size, glyph_buffer);
  PSF1_FONT* complete_font;
  SystemTable->BootServices->AllocatePool(EfiLoaderData, sizeof(PSF1_FONT), reinterpret_cast<void**>(&complete_font));
  complete_font->psf1_header = font_header;
  complete_font->glyph_buffer = glyph_buffer;
  return complete_font;
}

#endif // UEFI_IMPL_RUNTIME_PSF1_HELPER_H
