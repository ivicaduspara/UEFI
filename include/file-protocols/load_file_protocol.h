#ifndef UEFI_IMPL_LOAD_FILE_PROTOCOL_H
#define UEFI_IMPL_LOAD_FILE_PROTOCOL_H
#include "../device-path-protocols/device_path_protocol.h"
#define EFI_LOAD_FILE_PROTOCOL_GUID {0x56EC3091,0x954C,0x11d2,{0x8e,0x3f,0x00,0xa0, 0xc9,0x69,0x72,0x3b}}

EFI_GUID create_efi_load_file_protocol_GUID() {
  return EFI_GUID{0x56EC3091,0x954C,0x11d2,{0x8e,0x3f,0x00,0xa0, 0xc9,0x69,0x72,0x3b}};
}

struct EFI_LOAD_FILE_PROTOCOL;

using EFI_LOAD_FILE = EFI_STATUS (*)(EFI_LOAD_FILE_PROTOCOL* This, EFI_DEVICE_PATH_PROTOCOL* FilePath,
                                     BOOLEAN BootPolicy, UINTN* BufferSize, void* Buffer);

struct EFI_LOAD_FILE_PROTOCOL { //NOLINT
  EFI_LOAD_FILE LoadFile;
};
#endif //UEFI_IMPL_LOAD_FILE_PROTOCOL_H
