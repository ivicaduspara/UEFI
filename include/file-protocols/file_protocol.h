#ifndef UEFI_IMPL_FILE_PROTOCOL_H
#define UEFI_IMPL_FILE_PROTOCOL_H
#include "../base/efi_time_services.h"
#include "../base/GUID.h"

#define EFI_FILE_MODE_READ      0x0000000000000001
#define EFI_FILE_MODE_WRITE     0x0000000000000002
#define EFI_FILE_MODE_CREATE    0x8000000000000000
// File Attributes
#define EFI_FILE_READ_ONLY      0x0000000000000001
#define EFI_FILE_HIDDEN         0x0000000000000002
#define EFI_FILE_SYSTEM         0x0000000000000004
#define EFI_FILE_RESERVED       0x0000000000000008
#define EFI_FILE_DIRECTORY      0x0000000000000010
#define EFI_FILE_ARCHIVE        0x0000000000000020
#define EFI_FILE_VALID_ATTR     0x0000000000000037

#define EFI_FILE_INFO_GUID {0x09576e92,0x6d3f,0x11d2,{0x8e39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}

EFI_GUID create_efi_file_info_GUID() {
  return {0x09576e92,0x6d3f,0x11d2,{0x8e, 0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}};
}
using EFI_EVENT = void*;
// UEFI 2.9 specs, page 520
struct EFI_FILE_IO_TOKEN {
  EFI_EVENT   Event;
  EFI_STATUS  Status;
  UINTN       BufferSize;
  void*       Buffer;
};

//UEFI 2.9 specs, page 530
struct EFI_FILE_INFO {
  UINT64      Size;
  UINT64      FileSize;
  UINT64      PhysicalSize;
  EFI_TIME    CreateTime;
  EFI_TIME    LastAccessTime;
  EFI_TIME    ModificationTime;
  UINT64      Attribute;
  CHAR16      FileName[];
};

struct EFI_FILE_PROTOCOL;

using EFI_FILE_OPEN = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, EFI_FILE_PROTOCOL** NewHandle,
                                     CHAR16* FileName, UINT64 OpenMode, UINT64 Attributes);
using EFI_FILE_CLOSE = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This);
using EFI_FILE_DELETE = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This);
using EFI_FILE_READ =  EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, UINTN* BufferSize, void* Buffer);
using EFI_FILE_WRITE = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, UINTN* BufferSize, void* Buffer);
using EFI_FILE_GET_POSITION = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, UINT64* Position);
using EFI_FILE_SET_POSITION = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, UINT64 Position);
using EFI_FILE_GET_INFO = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, void* InformationType, UINTN* BufferSize, void* Buffer);
using EFI_FILE_SET_INFO = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, EFI_GUID* InformationType, UINTN BufferSize, void* Buffer);
using EFI_FILE_FLUSH = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This);
using EFI_FILE_OPEN_EX = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, EFI_FILE_PROTOCOL** NewHandle,
                                        CHAR16* FileName, UINT64 OpenMode, UINT64 Attributes, EFI_FILE_IO_TOKEN* Token);
using EFI_FILE_READ_EX = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, EFI_FILE_IO_TOKEN* Token);
using EFI_FILE_WRITE_EX = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, EFI_FILE_IO_TOKEN* Token);
using EFI_FILE_FLUSH_EX = EFI_STATUS (*)(EFI_FILE_PROTOCOL* This, EFI_FILE_IO_TOKEN* Token);

// UEFI 2.9 Specs PDF Page 512
struct EFI_FILE_PROTOCOL {
  UINT64                  Revision;
  EFI_FILE_OPEN           Open;
  EFI_FILE_CLOSE          Close;
  EFI_FILE_DELETE         Delete;
  EFI_FILE_READ           Read;
  EFI_FILE_WRITE          Write;
  EFI_FILE_GET_POSITION   GetPosition;
  EFI_FILE_SET_POSITION   SetPosition;
  EFI_FILE_GET_INFO       GetInfo;
  EFI_FILE_SET_INFO       SetInfo;
  EFI_FILE_FLUSH          Flush;
  EFI_FILE_OPEN_EX        OpenEx;
  EFI_FILE_READ_EX        ReadEx;
  EFI_FILE_WRITE_EX       WriteEx;
  EFI_FILE_FLUSH_EX       FlushEx;
};

#endif //UEFI_IMPL_FILE_PROTOCOL_H
