#ifndef UEFI_IMPL_FILE_SYSTEM_PROTOCOL_H
#define UEFI_IMPL_FILE_SYSTEM_PROTOCOL_H
#include "file_protocol.h"
#include "load_file_protocol.h"
//UEFI 2.9 specs, page 510

#define EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID {0x0964e5b22,0x6459,0x11d2,{0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}

EFI_GUID create_efi_simple_file_system_protocol_GUID() {
  return {0x0964e5b22,0x6459,0x11d2,{0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}};
}

struct EFI_SIMPLE_FILE_SYSTEM_PROTOCOL;

using OPEN_VOLUME = EFI_STATUS (*)(EFI_SIMPLE_FILE_SYSTEM_PROTOCOL* This, EFI_FILE_PROTOCOL** Root);

struct EFI_SIMPLE_FILE_SYSTEM_PROTOCOL { //NOLINT
  UINT64                                      Revision;
  OPEN_VOLUME OpenVolume;
};

#endif //UEFI_IMPL_FILE_SYSTEM_PROTOCOL_H
