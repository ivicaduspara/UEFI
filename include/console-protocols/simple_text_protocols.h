#ifndef UEFI_IMPL_SIMPLE_TEXT_PROTOCOLS_H
#define UEFI_IMPL_SIMPLE_TEXT_PROTOCOLS_H
#include "../base/GUID.h"
#include "../base/efi_status_codes.h"

/*
 * Contains definitions/declarations of EFI_SIMPLE_TEXT_INPUT/OUTPUT_PROTOCOL structures
 * */
//UEFI 2.9 spec, page 452
//*******************************************************
// UNICODE DRAWING CHARACTERS
//*******************************************************
#define BOXDRAW_HORIZONTAL                        0x2500
#define BOXDRAW_VERTICAL                          0x2502
#define BOXDRAW_DOWN_RIGHT                        0x250c
#define BOXDRAW_DOWN_LEFT                         0x2510
#define BOXDRAW_UP_RIGHT                          0x2514
#define BOXDRAW_UP_LEFT                           0x2518
#define BOXDRAW_VERTICAL_RIGHT                    0x251c
#define BOXDRAW_VERTICAL_LEFT                     0x2524
#define BOXDRAW_DOWN_HORIZONTAL                   0x252c
#define BOXDRAW_UP_HORIZONTAL                     0x2534
#define BOXDRAW_VERTICAL_HORIZONTAL               0x253c
#define BOXDRAW_DOUBLE_HORIZONTAL                 0x2550
#define BOXDRAW_DOUBLE_VERTICAL                   0x2551
#define BOXDRAW_DOWN_RIGHT_DOUBLE                 0x2552
#define BOXDRAW_DOWN_DOUBLE_RIGHT                 0x2553
#define BOXDRAW_DOUBLE_DOWN_RIGHT                 0x2554
#define BOXDRAW_DOWN_LEFT_DOUBLE                  0x2555
#define BOXDRAW_DOWN_DOUBLE_LEFT                  0x2556
#define BOXDRAW_DOUBLE_DOWN_LEFT                  0x2557
#define BOXDRAW_UP_RIGHT_DOUBLE                   0x2558
#define BOXDRAW_UP_DOUBLE_RIGHT                   0x2559
#define BOXDRAW_DOUBLE_UP_RIGHT                   0x255a
#define BOXDRAW_UP_LEFT_DOUBLE                    0x255b
#define BOXDRAW_UP_DOUBLE_LEFT                    0x255c
#define BOXDRAW_DOUBLE_UP_LEFT                    0x255d
#define BOXDRAW_VERTICAL_RIGHT_DOUBLE             0x255e
#define BOXDRAW_VERTICAL_DOUBLE_RIGHT             0x255f
#define BOXDRAW_DOUBLE_VERTICAL_RIGHT             0x2560
#define BOXDRAW_VERTICAL_LEFT_DOUBLE              0x2561
#define BOXDRAW_VERTICAL_DOUBLE_LEFT              0x2562
#define BOXDRAW_DOUBLE_VERTICAL_LEFT              0x2563
#define BOXDRAW_DOWN_HORIZONTAL_DOUBLE            0x2564
#define BOXDRAW_DOWN_DOUBLE_HORIZONTAL            0x2565
#define BOXDRAW_DOUBLE_DOWN_HORIZONTAL            0x2566
#define BOXDRAW_UP_HORIZONTAL_DOUBLE              0x2567
#define BOXDRAW_UP_DOUBLE_HORIZONTAL              0x2568
#define BOXDRAW_DOUBLE_UP_HORIZONTAL              0x2569
#define BOXDRAW_VERTICAL_HORIZONTAL_DOUBLE        0x256a
#define BOXDRAW_VERTICAL_DOUBLE_HORIZONTAL        0x256b
#define BOXDRAW_DOUBLE_VERTICAL_HORIZONTAL        0x256c
//*******************************************************
// EFI Required Block Elements Code Chart
//*******************************************************
#define BLOCKELEMENT_FULL_BLOCK                   0x2588
#define BLOCKELEMENT_LIGHT_SHADE                  0x2591
//*******************************************************
// EFI Required Geometric Shapes Code Chart
//*******************************************************
#define GEOMETRICSHAPE_UP_TRIANGLE                0x25b2
#define GEOMETRICSHAPE_RIGHT_TRIANGLE             0x25ba
#define GEOMETRICSHAPE_DOWN_TRIANGLE              0x25bc
#define GEOMETRICSHAPE_LEFT_TRIANGLE              0x25c4
//*******************************************************
// EFI Required Arrow shapes
//*******************************************************
#define ARROW_UP                                  0x2191
#define ARROW_DOWN                                0x2193

#define EFI_SIMPLE_TEXT_INPUT_PROTOCOL_GUID \
{0x387477c1,0x69c7,0x11d2,\
{0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}

#define EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL_GUID \
{0x387477c2,0x69c7,0x11d2,\
{0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}


inline EFI_GUID create_simple_text_input_protocol_GUID() {
  return EFI_GUID{0x387477c1, 0x69c7, 0x11d2, {0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}};
}

inline EFI_GUID create_simple_text_output_protocol_GUID() {
  return EFI_GUID{0x387477c2, 0x69c7, 0x11d2, {0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}};
}


//Forward declare SIMPLE_TEXT_INPUT/OUTPUT_PROTOCOLS so functions can be aliased.
struct EFI_SIMPLE_TEXT_INPUT_PROTOCOL;
struct EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL;

//UEFI 2.9 spec, page 448
struct EFI_INPUT_KEY {
  UINT16      ScanCode;
  CHAR16      UnicodeChar;
};

inline BOOLEAN Check_Keystroke_Value(const EFI_INPUT_KEY& key_stroke, CHAR16 unicode_char) {
  return key_stroke.UnicodeChar == unicode_char;
}

//Aliases for EFI_SIMPLE_TEXT_INPUT_PROTOCOL's functions.
using EFI_INPUT_RESET = EFI_STATUS(*)(EFI_SIMPLE_TEXT_INPUT_PROTOCOL* This, BOOLEAN ExtendedVerification);
using EFI_INPUT_READ_KEY = EFI_STATUS(*)(EFI_SIMPLE_TEXT_INPUT_PROTOCOL* This, EFI_INPUT_KEY* Key);

//UEFI 2.9 spec, page 446
struct EFI_SIMPLE_TEXT_INPUT_PROTOCOL {
  EFI_INPUT_RESET         Reset;
  EFI_INPUT_READ_KEY      ReadKeyStroke;
  EFI_EVENT               WaitForKey;
};

//Aliases for EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL's functions.
using EFI_TEXT_RESET = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, BOOLEAN ExtendedVerification);
using EFI_TEXT_STRING = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, CHAR16* String);
using EFI_TEXT_TEST_STRING = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, CHAR16* String);
using EFI_TEXT_QUERY_MODE = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, UINTN ModeNumber, UINTN* Columns, UINTN* Rows);
using EFI_TEXT_SET_MODE = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, UINTN ModeNumber);
using EFI_TEXT_SET_ATTRIBUTE = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, UINTN Attribute);
using EFI_TEXT_CLEAR_SCREEN = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This);
using EFI_TEXT_SET_CURSOR_POSITION = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, UINTN Column, UINTN Row);
using EFI_TEXT_ENABLE_CURSOR = EFI_STATUS(*)(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* This, BOOLEAN Visible);

//UEFI 2.9 spec, page 450
struct SIMPLE_TEXT_OUTPUT_MODE{
  INT32     MaxMode;
  INT32     Mode;
  INT32     Attribute;
  INT32     CursorColumn;
  INT32     CursorRow;
  BOOLEAN   CursorVisible;
};

//UEFI 2.9 spec, page 449
struct EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL {
  EFI_TEXT_RESET                    Reset;
  EFI_TEXT_STRING                   OutputString;
  EFI_TEXT_TEST_STRING              TestString;
  EFI_TEXT_QUERY_MODE               QueryMode;
  EFI_TEXT_SET_MODE                 SetMode;
  EFI_TEXT_SET_ATTRIBUTE            SetAttribute;
  EFI_TEXT_CLEAR_SCREEN             ClearScreen;
  EFI_TEXT_SET_CURSOR_POSITION      SetCursorPosition;
  EFI_TEXT_ENABLE_CURSOR            EnableCursor;
  SIMPLE_TEXT_OUTPUT_MODE*          Mode;
};

inline void print_unsigned_number(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* console, UINT64 num) {
  if (num == 0) {
    console->OutputString(console, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"0"));
    return;
  }
  UINT64 buffer[19];
  UINT64 index = 18;
  while (num != 0) {
    buffer[index] = num % 10;
    index--;
    num/= 10;
  }
  //Index is now pointing to "one digit before" the most significant digit.
  CHAR16 print_buffer[19];
  for(CHAR16 i = 0; i < 19; i++) {
    print_buffer[i] = 0x0000;
  }
  for(UINT64 i = 18; i > index; i--) {
    print_buffer[18 - i] = static_cast<CHAR16>(buffer[index + 1 + (18 - i)] + '0');
  }
  console->OutputString(console, print_buffer);
}

inline void print_unsigned_hex_number(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* console, UINT64 num) {
  if (num == 0) {
    console->OutputString(console, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"0"));
    return;
  }
  UINT64 buffer[19];
  UINT64 index = 18;
  while (num != 0) {
    buffer[index] = num % 16;
    index--;
    num/= 16;
  }
  //Index is now pointing to "one digit before" the most significant digit.
  CHAR16 print_buffer[19];
  for(CHAR16 i = 0; i < 19; i++) {
    print_buffer[i] = 0x0000;
  }
  for(UINT64 i = 18; i > index; i--) {
    if (buffer[index + 1 + (18 - i)] < 10) {
      print_buffer[18 - i] = static_cast<CHAR16>(buffer[index + 1 + (18 - i)] + '0');
    } else if (buffer[index + 1 + (18 - i)] == 10) {
      print_buffer[18 - i] = static_cast<CHAR16>(65);
    } else if (buffer[index + 1 + (18 - i)] == 11) {
      print_buffer[18 - i] = static_cast<CHAR16>(66);
    } else if (buffer[index + 1 + (18 - i)] == 12) {
      print_buffer[18 - i] = static_cast<CHAR16>(67);
    } else if (buffer[index + 1 + (18 - i)] == 13) {
      print_buffer[18 - i] = static_cast<CHAR16>(68);
    } else if (buffer[index + 1 + (18 - i)] == 14) {
      print_buffer[18 - i] = static_cast<CHAR16>(69);
    } else if (buffer[index + 1 + (18 - i)] == 15) {
      print_buffer[18 - i] = static_cast<CHAR16>(70);
    }

  }
  console->OutputString(console, print_buffer);
}

inline void print_signed_number(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* console, INT64 num) {
  if (num == 0) {
    console->OutputString(console, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"0"));
    return;
  }
  BOOLEAN negative = num < 0;
  INT64 buffer[19];
  INT64 index = 18;
  while (num != 0) {
    buffer[index] = num % 10;
    index--;
    num/=10;
  }
  CHAR16 print_buffer[19];
  for(CHAR16 i = 0; i < 19; i++) {
    print_buffer[i] = 0x0000;
  }
  for(INT64 i = 18; i >= index; i--) {
    print_buffer[18 - i] = static_cast<CHAR16>(buffer[index + 1 + (18 - i)] + '0');
  }
  console->OutputString(console, print_buffer);
}

inline void print_error(EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* ConOut, CHAR16* text, EFI_STATUS status) {
  ConOut->SetAttribute(ConOut, EFI_RED);
  ConOut->OutputString(ConOut, text);
  ConOut->OutputString(ConOut, CheckStandardEFIError(status));
  ConOut->OutputString(ConOut, CAST_CPP_WCHAR_STAR_TO_UEFI_CHAR16_STAR(L"\r\n"));
  ConOut->SetAttribute(ConOut, EFI_WHITE);
}

#endif //UEFI_IMPL_SIMPLE_TEXT_PROTOCOLS_H
