#ifndef UEFI_IMPL_BOOTINFOSTRUCT_H
#define UEFI_IMPL_BOOTINFOSTRUCT_H
#include "system-table/efi_system_table.h"
#include "helper-interfaces/runtime_graphics_helper_interface.h"
#include "helper-interfaces/runtime_psf1_helper.h"

struct BootInfoStruct {
  Frame_Buffer frame_buffer_;
  PSF1_FONT* psf1Font_;
  EFI_MEMORY_DESCRIPTOR* efi_memory_descriptor_;
  EFI_SYSTEM_TABLE* efi_system_table_;
  UINTN map_key_;
  UINTN memory_map_size_;
  UINTN memory_descriptor_size_;
};
#endif // UEFI_IMPL_BOOTINFOSTRUCT_H
